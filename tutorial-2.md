---
title: 'Gamedev in Lisp. Part 2: Dungeons and Interfaces'
---


In [Part 1](tutorial-1), we explored the Entity-Component-System architectural pattern, commonly used in game development, and the metalinguistic programming paradigm, which involves building and using custom languages that describe the problem domain of the program being created as precisely and completely as possible. This time, we will use these techniques to create a small but fully-fledged game in the genre of dungeon crawler with a user interface, and we will use it as an example to examine the system design of a real game application using ECS.

TL;DR: a ready-to-run demo (binaries for all common OSes) and its source code could be found on [github](https://github.com/lockie/ecs-tutorial-2).

---

If necessary, refresh your knowledge of Common Lisp syntax using the short guide [Learn X in Y minutes, Where X=Common Lisp](https://learnxinyminutes.com/docs/common-lisp), or the more detailed [Practical Common Lisp](https://gigamonkeys.com/book) book. Assuming we have already set up a working environment for gamedev in Common Lisp following the [instructions](tutorial-1#development-environment) from Part 1, let's update the local versions of the installed packages by launching the SBCL compiler's REPL with the `sbcl` command in terminal and executing the following code:

```lisp
(ql-util:without-prompting (ql:update-all-dists))
```

Then, just like last time, we will start by using the [cookiecutter template](https://github.com/lockie/cookiecutter-lisp-game):

```sh
cookiecutter gh:lockie/cookiecutter-lisp-game
```

Respond "yes" to the cookiecutter prompt "*Is it okay to delete and re-download it?*" in order to download the updated version of the game template, and answer the questions about the new project as follows:

```
full_name (Your Name): Alyssa P. Hacker
email (your@e.mail): alyssa@domain.tld
project_name (The Game): ECS Tutorial 2
project_slug (ecs-tutorial-2):
project_short_description (A simple game.): cl-fast-ecs framework tutorial.
version (0.0.1):
Select backend
    1 - liballegro
    2 - raylib
    3 - SDL2
    Choose from [1/2/3] (1): 1
```

Again, let's add a link to the project directory to our local Quicklisp package repository:

```sh
ln -s $(pwd)/ecs-tutorial-2 $HOME/quicklisp/local-projects/  # for UNIX-like OS

mklink /j %USERPROFILE%\quicklisp\local-projects\ecs-tutorial-2 ecs-tutorial-2  # for Windows
mklink /j %USERPROFILE%\portacle\projects\ecs-tutorial-2 ecs-tutorial-2  # for Windows using Portacle
```

We'll immediately increase the size of the game window to 1280⨉800 to cover a larger area of the dungeon by replacing the values of constants  `+window-width+` и `+window-height+` at the beginning of the`src/main.lisp` file (in Common Lisp, it is customary to put a `+` sign at the beginning and the end of constant names to clearly indicate that they are not variables, which is perfectly [syntactically](https://cl-community-spec.github.io/pages/002b.html) correct):

```lisp
(define-constant +window-width+ 1280)
(define-constant +window-height+ 800)
```

Finally, let's make sure the new project works by running `sbcl` and executing the following code in it:

```lisp
(ql:quickload :ecs-tutorial-2)
(ecs-tutorial-2:main)
```

As a result, we will see a black window with the specified resolution and a lonely FPS counter:

![](uploads/ba77fab6840066c3e9ddd9f34cb8c333/start.png)

## Dungeons

To allow the player to crawl in the dungeon, the first thing we need is a dungeon map.

![](uploads/4ccdab216baaf33e4293d510f12aba87/map.jpg)

For that, we'll use the excellent open-source map editor [Tiled](https://thorbjorn.itch.io/tiled), which is not only cross-platform, but also cross-engine, as it saves map data in trivial XML, supported literally everywhere, including, of course, Common Lisp. Moreover, there is a library called [cl-tiled](https://github.com/Zulu-Inuoe/cl-tiled) which loads Tiled files as Lisp objects, and we will be using it.

For a quick start, you can begin with a simple dungeon map using a set of map elements (so-called *tileset*) [Dungeon Tileset II - Extended](https://nijikokun.itch.io/dungeontileset-ii-extended), which looks something like this in the editor:

![](uploads/7b4962fea2af21856e390cd5227c2eed/map-final.png)

You can download the ready-to-use map files (the tileset and the Tiled file `level1.tmx` with XML inside) [here](uploads/3bc619472e2723fe3c70ce252c37a0f5/Resources.zip), but let's go through how to achieve the same result. First, we need to resize the tileset, as 16⨉16 pixels is a bit too small. Let's double it by downloading the [archive `dungeontiles-extended v1.1.zip`](https://nijikokun.itch.io/dungeontileset-ii-extended/purchase) (you can donate to the tileset's author if you wish, or download it for free by clicking *"No thanks, just take me to the downloads"* link), unpack the file `dungeontileset-extended.png` from the archive into the `Resources` directory of our project, and process it using [ImageMagick](https://imagemagick.org/script/download.php) with the following command:

```sh
convert dungeontileset-extended.png -filter box -resize 200% dungeontileset-extended2.png
```

We can create the map file by launching Tiled and first selecting *File 🡒 New 🡒 New Project...* from the menu, saving the map project in the `Resources` directory, for example, as `dungeon.tiled-project`. Then, select *File 🡒 New 🡒 New Map...*, choosing the following options in the map creation dialog:

![](uploads/852fbe307a828e353e33253afd082daf/new-map-en.png)

You can save the map in the same `Resources` directory under a name like `level1.tmx`. Next, we need to let Tiled know about our awesome tileset. For that, select *File 🡒 New 🡒 New Tileset...* and in the dialog that opens, specify the width and height of the tiles, then click *Browse* in the *Image* section and select the `dungeontileset-extended2.png` file:

![](uploads/c9feb5bd7eab0e8f34d1f2febb8c81da/new-tileset-en.png)

Save the resulting tileset under the suggested name `dungeontileset-extended2.tsx` in the familiar `Resources` directory.

After that, we can finally unleash our inner artist and start placing tiles onto the blank map canvas by selecting them from the *Tilesets* window:

![](uploads/67509bb3e32ce66ef8ccc35f21c7448e/map-drawing-en.png)

Detailed documentation on the Tiled editor, its features and tools can be found on the [official website](https://doc.mapeditor.org/en/stable). Remember, the ready-to-use map files for our project can be downloaded [here](uploads/3bc619472e2723fe3c70ce252c37a0f5/Resources.zip).

Once we've prepared the map files, we can load it into our game. To do this, we need to take a few preparatory steps. First, let's add the `cl-tiled` library dependency to the game Lisp system definition file `ecs-tutorial-2.asd` located in the root of our project:

```lisp
  ;; ...
  :license "MIT"
  :depends-on (#:alexandria
               #:cl-liballegro
               #:cl-liballegro-nuklear
               #:cl-tiled               ;; modify here
               #:livesupport)
  ;; ...
```

Next, in the same file, we'll add a reference to a new file `map.lisp`, which we'll also create in the `src` subdirectory of our project:

```lisp
  ;; ...
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "map")      ;; here
                 (:file "main"))))
  ;; ...
```

This new file will contain the code necessary for loading and displaying the map. Next, in the `src/package.lisp` file, add an alias for the `cl-tiled` library so we can refer to it simply as `tiled` in the code:

```lisp
(defpackage #:ecs-tutorial-2
  (:use #:cl)
  (:import-from #:alexandria #:define-constant)
  (:local-nicknames (#:tiled #:cl-tiled))        ;; here
  (:export #:main))
```

Finally, let's load the updated system definition by executing the following code in REPL: `(ql:quickload :ecs-tutorial-2)`.

Now we face a conceptual dilemma. The thing is that the `cl-tiled` library returns the map data as CLOS-based objects (*Common Lisp Object System*, a set of object-oriented language mechanisms), which are very convenient to explore in REPL, just take a look at this:

![](uploads/987c3fe3805b459e487ca688b760554a/map-inspect.png)


However, using these objects directly in the game can lead to performance issues, since working with them often involves [runtime dispatching](https://en.wikipedia.org/wiki/Dynamic_dispatch), conceptually similar to calling virtual methods in C++. And we will in fact have at least a thousand such objects (40⨉25 tiles 32⨉32 pixels each to fill the 1280⨉800 window). You can experience the performance drawbacks of this approach yourself by running [this demo](https://github.com/lockie/cl-tiled-demo). On a 12-core Ryzen 5 3600, enabling map rendering in the `render` function causes FPS to drop from 20,000 to 600. This means each frame takes `1/600 - 1/20000 = 0.0016` seconds, or over one and a half milliseconds, and this is for a map with just one layer of 40⨉25 tiles!

To solve this problem, we can leverage the Entity-Component-System pattern, which we discussed in detail in [Part 1](tutorial-1). We will move the map tile data, kindly loaded for us by the `cl-tiled` library, into the ECS storage. This will not only boost performance by eliminating dispatching and making better use of the CPU cache, but it will also allow us to break down the tasks of rendering and processing the map content into smaller, conceptually independent pieces. We'll do this by properly defining the data we are interested in (components) and the code that processes it (systems).

Let's begin by adding the `cl-fast-ecs` framework to our project. We'll include it in the dependency list in the system definition file `ecs-tutorial-2.asd`:

```lisp
  ;; ...
  :license "MIT"
  :depends-on (#:alexandria
               #:cl-fast-ecs            ;; here
               #:cl-liballegro
               #:cl-liballegro-nuklear
               #:cl-tiled
               #:livesupport)
  ;; ...
```

...and then reload the updated game system definition by running `(ql:quickload :ecs-tutorial-2)` in REPL.

Now in `src/main.lisp` we can update the `init` function to initialize the ECS framework by calling the [`bind-storage`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ABIND-STORAGE) function, and modify the `update` function to run registered ECS systems by calling [`run-systems`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ARUN-SYSTEMS):


```lisp
;; ...

(defun init ()
  (ecs:bind-storage))

;; ...

(defun update (dt)
  (unless (zerop dt)
    (setf *fps* (round 1 dt)))
  (ecs:run-systems :dt (float dt 0.0)))  ;; here
```

Next, using the [`ecs:defcomponent`](https://lockie.gitlab.io/cl-fast-ecs/#MACRO-FUNCTION%20CL-FAST-ECS%3ADEFCOMPONENT) macro calls in the `src/map.lisp` file, let's add a *tag component* (a component without data) `map`, which will simply mark the entity of the loaded map, and a `map-tile` component corresponding to an individual tile on the map. We'll start the file by activating the necessary [package](https://lisp-docs.github.io/docs/tutorial/projects/packages-systems) `ecs-tutorial-2` (all subsequent definitions of functions and variables will be created in this package). Later on, some tiles will represent impassable obstacles, such as walls or closed doors, so we add a Boolean slot named `obstacle` to `map-tile` component indicating whether the tile is an obstacle or not, with a default value of [`nil`](https://cl-community-spec.github.io/pages/NILv.html), i.e. "false":

```lisp
(in-package #:ecs-tutorial-2)


(ecs:defcomponent map)

(ecs:defcomponent map-tile
  (obstacle nil :type boolean))
```

We'll also introduce a `parent` component to emphasize the fact that all tiles are child objects of the map entity (from now on we will use the term "object" in the sense of "an entity with some components", completely discarding OOP connotations):

```lisp
(ecs:defcomponent parent
  (entity -1 :type ecs:entity :index children))
```

We'll add this component to all tiles and other map-related objects, with the `entity` slot set to the map entity. For easier memory management, we want to be able to delete all map-related objects in one fell swoop when the map entity is deleted. To make this happen we'll need to track child objects of a given entity, but doing so directly would be costly (up to *O(n)*, where *n* is the number of entities, and we will have at least a thousand of them, as we calculated above). To handle this efficiently, we'll use *indices*, a feature in the `cl-fast-ecs` framework that is not quite typical for the ECS pattern. An index is a function kindly generated by the framework with a a name given by `index` argument in the slot description, which will upon the call answer the question "which entities have a particular given value in this slot?". Index functions are built on top of open-address hash tables, so they work out for amort. *O(1)*, but they are not as cache friendly as basic ECS mechanisms. Additionally, just like in industrial RDBMSes, they increase the overhead of creating and deleting the component since the index must be updated in these cases, so we should not overuse this mechanism.

We name the index function `children`, and in full accordance with its name, it will take an entity (recall that in `cl-fast-ecs`, as in many other ECS implementations, entities are represented by integers), and it will return a list with entities whose `entity` slot in the `parent` component is equal to the specified one, i.e. a list of its child objects.

Now to ensure that child objects are automatically deleted, we will call the [`ecs:hook-up`](https://lockie.gitlab.io/cl-fast-ecs/#MACRO-FUNCTION%20CL-FAST-ECS%3AHOOK-UP) function in `src/map.lisp` to extend the [`*entity-deleting-hook*`](https://lockie.gitlab.io/cl-fast-ecs/#VARIABLE%20CL-FAST-ECS%3A%2AENTITY-DELETING-HOOK%2A) with [anonymous function](https://cl-community-spec.github.io/pages/lambda.html) that will loop through the list of child entities using the `children` index function and the standard [dolist](https://cl-community-spec.github.io/pages/dolist.html) macro, and delete each one with [`ecs:delete-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ADELETE-ENTITY):

```lisp
(ecs:hook-up ecs:*entity-deleting-hook*
             (lambda (entity)
               (dolist (child (children entity))
                 (ecs:delete-entity child))))
```

Next, we define the component responsible for images, which will store only a C pointer to an [`ALLEGRO_BITMAP`](https://liballeg.org/a5docs/trunk/graphics.html#allegro_bitmap) structure from the [liballegro](https://liballeg.org) middleware library we're using:

```lisp
(ecs:defcomponent image
  (bitmap (cffi:null-pointer) :type cffi:foreign-pointer))
```

We'll store the 32⨉32 fragments of the original tileset image created by the calls to [`al_create_sub_bitmap`](https://liballeg.org/a5docs/trunk/graphics.html#al_create_sub_bitmap) in its `bitmap` slot.

Later on, we'll face another conceptual problem. On the one hand, we have individual tiles as specific fragments of the image `dungeontileset-extended2.png` with size 32⨉32 and optional custom properties (more on that later). On the other hand, this tile can occur on the map itself more than once at different coordinates, for example, if it is a repeating texture of a dungeon wall or floor. This is somewhat similar to OOP: there are classes (tiles from the tileset), and there are instances of those classes (tiles on the map). To handle this efficiently and avoid re-loading images for each tile on the map, we introduce the concept of a tile *prefab* (from "pre-fabricated"). When loading the tileset, we will create entities with prefab and image components. These will just store the tile data and won't be rendered later. Next, each tile on the map will be represented by a separate entity with position component and image component. When loading the tile, we will simply copy data to the latter from the `image` component of corresponding tile prefab — in Tiled each tile in tileset has a unique numeric identifier. We don't have to worry about the redundant `image` component, since it is basically one pointer occupying extra 8 bytes of memory per map tile. So, let's define the prefab component with a unique identifier:

```lisp
(ecs:defcomponent map-tile-prefab
  (gid 0 :type fixnum :index map-tile-prefab :unique t))
```

Since we will often have to search for a prefab by its numeric ID when loading a map, and again this is *O(n)* in time, we use the index function here as well, also naming it `map-tile-prefab`. Additionally, we pass the `unique` argument for the index with a Boolean truth value, [`t`](https://cl-community-spec.github.io/pages/t.html), which tells the framework that the values of this slot are unique there can't be more than one entity with a particular ID value. This makes the index function return a single entity rather than a list.

Besides, we interact with the C library `liballegro` to load and render images, and C is a language with manual memory management, which imposes on us the obligation to correctly dispose image resources, the Lisp garbage collector will be powerless here. So let's use another new mechanism of the `cl-fast-ecs` framework, the *finalizer*, which is a function that is automatically called when a particular component is deleted. Let's modify the definition of the `image` component as follows:

```lisp
(ecs:defcomponent (image :finalize (lambda (entity &key bitmap)            ;; modify here
                                     (when (has-map-tile-prefab-p entity)  ;;
                                       (al:destroy-bitmap bitmap))))       ;;
  (bitmap (cffi:null-pointer) :type cffi:foreign-pointer))
```

Note that we are removing the image structure [`ALLEGRO_BITMAP`](https://liballeg.org/a5docs/trunk/graphics.html#allegro_bitmap) by calling the function [`al_destroy_bitmap`](https://liballeg.org/a5docs/trunk/graphics.html#al_destroy_bitmap) only when the entity having the `image` component removed also has a tile prefab component — we check this by calling the `has-map-tile-prefab-p` predicate generated for us by the framework and the standard [`when`](https://cl-community-spec.github.io/pages/when.html) macro. This way we avoid the good old C memory error called *double free*, because we can have many tiles on the map with the same pointer to [`ALLEGRO_BITMAP`](https://liballeg.org/a5docs/trunk/graphics.html#allegro_bitmap) copied from a common prefab.

Finally, we define the trivial `position` and `size` components responsible for the objects' location and size in pixels:

```lisp
(ecs:defcomponent position
  (x 0.0 :type single-float)
  (y 0.0 :type single-float))

(ecs:defcomponent size
  (width  0.0 :type single-float)
  (height 0.0 :type single-float))
```

Note that `liballegro` represents screen coordinates as single precision floating point numbers for reasons of compatibility with raw OpenGL, and we do the same. We'll also follow `liballegro` in considering the position of the image to be the coordinates of its upper left corner.

The data storage architecture we've built will look as follows (vertical rectangles represent entities, horizontal rectangles represent components):

![](uploads/17d7849be1b3e6bc38de16be67662788/tiles-and-prefabs.png)


Now we have all the basic components, and we can use the [`ecs:defsystem`](https://lockie.gitlab.io/cl-fast-ecs/#MACRO-FUNCTION%20CL-FAST-ECS%3ADEFSYSTEM) macro to define our first system in the project called `render-images` which will render all required images:

```lisp
(ecs:defsystem render-images
  (:components-ro (position image)
   :initially (al:hold-bitmap-drawing t)
   :finally (al:hold-bitmap-drawing nil))
  (al:draw-bitmap image-bitmap position-x position-y 0))
```

As in Part 1, we use the function [`al_hold_bitmap_drawing`](https://liballeg.org/a5docs/trunk/graphics.html#al_hold_bitmap_drawing) to activate `liballegro`'s built-in sprite batching to reduce unnecessary hardware draw calls. We also call [`al_draw_bitmap`](https://liballeg.org/a5docs/trunk/graphics.html#al_draw_bitmap) to actually draw the image at the specified coordinates. Note that according to our design, prefabs will not be processed by this system since they lack the `position` component.

However, in order to have something to draw, we need to implement loading the map data into components we've defined. Here's the function for this, `load-map`, along with the helper functions it uses:

```lisp
(defun load-bitmap (filename)
  (al:ensure-loaded #'al:load-bitmap (namestring filename)))

(defun tile->spec (tile bitmap map-entity)
  (let ((width  (tiled:tile-width tile))
        (height (tiled:tile-height tile)))
    (copy-list
     `((:parent :entity ,map-entity)
       (:image :bitmap ,(al:create-sub-bitmap bitmap
                                              (tiled:tile-pixel-x tile)
                                              (tiled:tile-pixel-y tile)
                                              width height))
       (:map-tile-prefab :gid ,(tiled:tile-gid tile))
       (:size :width ,(float width)
              :height ,(float height))))))

(defun load-tile-prefab (tile bitmap map-entity)
  (unless (ecs:entity-valid-p
           (map-tile-prefab (tiled:tile-gid tile) :missing-error-p nil))
    (let* ((internal-tile-spec (tile->spec tile bitmap map-entity))
           (tile-spec (ecs:spec-adjoin internal-tile-spec '(:map-tile)))
           (tile (ecs:make-object tile-spec)))
      tile)))

(defun load-tile (entity tile x y)
  (let ((prefab (map-tile-prefab (tiled:tile-gid tile))))
    (ecs:copy-entity prefab
                     :destination entity
                     :except '(:map-tile-prefab))
    (make-position entity :x (float x)
                          :y (float y))))

(defun load-map (filename)
  (let ((map (tiled:load-map filename))
        (map-entity (ecs:make-object '((:map)))))
    (dolist (tileset (tiled:map-tilesets map))
      (let ((bitmap (load-bitmap
                     (tiled:image-source (tiled:tileset-image tileset)))))
        (dolist (tile (tiled:tileset-tiles tileset))
          (load-tile-prefab tile bitmap map-entity))))
    (dolist (layer (tiled:map-layers map))
      (typecase layer
        (tiled:tile-layer
         (dolist (cell (tiled:layer-cells layer))
           (load-tile (ecs:make-entity) (tiled:cell-tile cell)
                      (tiled:cell-x cell) (tiled:cell-y cell))))))
    map-entity))
```

If you encounter errors about undefined symbols `copy-entity` or `spec-adjoin`, you may be using an outdated version of `cl-fast-ecs` from the main Quicklisp repository. The latest version with all the necessary features can be obtained from the [Lucky Lambda](http://dist.luckylambda.technology/releases/lucky-lambda/) repository using the instructions on its website.

Let's examine their code in detail.

The `load-bitmap` function is a wrapper around the image loading function from `liballegro`, [`al_load_bitmap`](https://liballeg.org/a5docs/trunk/graphics.html#al_load_bitmap), which thoroughly handles its error using the helper function [`al:ensure-loaded`](https://github.com/resttime/cl-liballegro/blob/eea31c9c949c1003d290db3f96564d9bffee0b8f/src/interface/streams.lisp#L15). The latter even provides an option to try to load a file with a different name by leveraging Common Lisp [mechanism](https://cl-community-spec.github.io/pages/Condition-System-Concepts.html) called *conditions*. A detailed book on this mechanism has recently been published by one of the prominent members of the Lisp community, [«The Common Lisp Condition System» by Michał "phoe" Herda](https://apress.com/us/book/9781484261330).

The `tile->spec` function creates a list with the object specification for a given tile from tileset, which will later be passed to the [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT) function of the ECS framework. The list is copied using the standard function [`copy-list`](https://cl-community-spec.github.io/pages/copy_002dlist.html) since we will modify it later and don't want to alter the original. The specification includes the following data:

* the map entity passed to the function in the `map-entity` parameter, which serves as parent entity in the `parent` component;
* the required tileset image fragment, obtained by calling [`al_create_sub_bitmap`](https://liballeg.org/a5docs/trunk/graphics.html#al_create_sub_bitmap), which is stored in the `bitmap` slot of the `image` component;
* the global tile ID, retrieved by calling the `tile-gid` function from `cl-tiled`, which is stored in the `map-tile-prefab` component;
* finally, we specify the tile size in `size` component (note that instead of hardcoding sizes, we make this parameter dynamic, following the [spirit of Lisp](https://github.com/Shinmera/talks/blob/master/gic2021-highly-dynamic/paper.pdf)).

The `load-tile-prefab` function loads a tile prefab. It checks if a prefab with the global ID returned by `tiled:tile-gid` already exists, using the standard macro [`unless`](https://cl-community-spec.github.io/pages/when.html), the index `map-tile-prefab` (remember, *O(1)* lookup time) and the function [`entity-valid-p`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AENTITY-VALID-P). The latter in current version of `cl-fast-ecs` simply checks if its argument is non-negative, but that's enough since the index functions with the `missing-error-p` argument set to [`nil`](https://cl-community-spec.github.io/pages/NILv.html) return `-1` instead of throwing an error. If the prefab was not created earlier, we generate its specification by calling `tile->spec` and add the `map-tile` component using the [`spec-adjoin`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ASPEC-ADJOIN) function from the ECS framework. Added in version 0.6.0, `spec-adjoin` modifies the object's specification by adding a new component if it wasn't already present. Finally, we pass the constructed specification to the [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT) function.

The `load-tile` function initializes a tile object using the prefab as a template. First, it retrieves the corresponding tile prefab by global ID returned by `tiled:tile-gid` using the already familiar index function `map-tile-prefab`, then it copies all prefab components, except for the `map-tile-prefab` itself, into the tile entity using the [`copy-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ACOPY-ENTITY) function, and finally creates a `position` component with the tile's coordinates on the map (remember, tiles differ from prefabs by having a position).

The `load-map` function immediately calls its namesake, the central function of the `cl-tiled` library — `tiled:load-map`, which returns the beautiful detailed CLOS object we saw in the earlier screenshot. It then creates a map entity via [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT), stores it in the variable `map-entity` and guts the CLOS object returned from `tiled:load-map` to extract the necessary data. First, it iterates through the list of tilesets returned by `tiled:map-tilesets` (there can be more than one) using the [`dolist`](https://cl-community-spec.github.io/pages/dolist.html) macro, calls the helper function `load-bitmap` for each tileset to load its image, and then iterates over its list of tiles using the same [`dolist`](https://cl-community-spec.github.io/pages/dolist.html), loading each one as a prefab with `load-tile-prefab` (remember, tiles in tileset are just templates, not actual map tiles). Second, `load-map` iterates over the list of tile layers (CLOS objects `tiled:tile-layer` returned by `tiled:map-layers`) and processes each layer's tiles, `tiled:layer-cells`. For each tile on the map, it creates a new entity via [`make-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-ENTITY), initializing all the required components using the `load-tiles` function discussed above. Finally, `load-map` returns the map entity, which serves as the parent entity for all tiles and their prefabs.

There's a subtlety in how we load the map, related to implicit ordering of tiles. In Tiled, a map can have a set of multiple layers, and this set is ordered — tiles from the layers "above" should be drawn on top of and cover tiles from the layers "below":

![](uploads/5f5cc0622b880ab41ae4e9a5a30b673d/map-layers-en.png)

We achieve this through two key factors. First, the layers in the list returned by `tiled:map-layers` are ordered exactly as they are in editor. Second, the [`make-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-ENTITY) function of ECS framework we've used to create tiles, guarantees that new entities are created in increasing order (recall that in ECS, entities are essentially integers indexing some internal arrays), and systems process entities in this same order, from the oldest to the most recently created. Thus, in the `render-images` system, we will draw individual tiles in the correct order, matching how they appear in the editor.

It's worth noting that our approach to handling map data is certainly not the only correct one. Actually, many ECS architecture proponents [do not recommend](https://reddit.com/r/roguelikedev/comments/9dxna3) storing every tile as a separate entity. We could, for example, render all the static map tiles into a memory buffer in the correct order during loading and then draw it all in one fell swoop. However, this would complicate code and cause new problems, and for this tutorial we choose the simplest working methods.

Now that we've examined the `load-map` function in detail, we can add its call with the appropriate argument to the `init` function in `src/map.lisp` file:

```lisp
(defun init ()
  (ecs:bind-storage)
  (load-map "level1.tmx"))  ;; here
```

After loading the accumulated system changes by calling `(ql:quickload :ecs-tutorial-2)` in REPL and launching our game with `(ecs-tutorial-2:main)`, we will see our map from the editor:

![](uploads/b062c9d924c5273f2c7e8699357f038f/map1.png)

### Animations

In addition to static images, Tiled also supports animated tiles, which can help bring our virtual dungeon to life. For example, you can add burning torches or fountains with some magical substances:

![](uploads/da9eee2a26ace5c7a6e4157d7a285a6a/fountain.gif)

To add an animated tile, open the tileset editor (using the gear button *Edit Tileset* in the *Tilesets* window), select the tile that will be the first frame of the animation, and open the animation editor by clicking the video camera button in the toolbar. In the window that appears, you can drag any tiles that should be the part of the animation into the left area and set the duration in milliseconds for each of them (or for all of them at once):

![](uploads/f069be28ead6aead5e9782b0eca1f8a9/animation-editor-en.png)

However, to display animations in our game, we need to extend and refactor its code. Let's start by adding two new files, `common.lisp` and `animation.lisp`, to the `src` subdirectory, and referencing them in `ecs-tutorial-2.asd`:

```lisp
  ;; ...
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "common")     ;; here
                 (:file "animation")  ;;
                 (:file "map")
                 (:file "main"))))
  ;; ...
```

In the former we will define components, functions and constants common to all systems (feel free to appreciate the irony in the file's name). In the latter, we'll define components and systems required for animations to function.

We'll begin the refactoring by moving the window size constants from `src/main.lisp` to `src/common.lisp`, making sure to start the file with the usual header with package selection:

```lisp
(in-package #:ecs-tutorial-2)


(define-constant +window-width+ 1280)  ;; don't forget to remove this from main.lisp
(define-constant +window-height+ 800)  ;;
```

Additionally, let's move the definitions of the `parent`, `position` and `size` components from `src/map.lisp` to `src/common.lisp`, as well as the entity deletion hook extension which deletes child objects:

```lisp
;; ...

;; don't forget to remove this from map.lisp

(ecs:defcomponent parent
  (entity -1 :type ecs:entity :index children))

(ecs:hook-up ecs:*entity-deleting-hook*
             (lambda (entity)
               (dolist (child (children entity))
                 (ecs:delete-entity child))))

(ecs:defcomponent position
  (x 0.0 :type single-float)
  (y 0.0 :type single-float))

(ecs:defcomponent size
  (width  0.0 :type single-float)
  (height 0.0 :type single-float))
```

Next, we'll move the definition of thee `image` component, the `render-images` system, and the `load-bitmap` function from `src/map.lisp` to the new `src/animation.lisp` file, also adding the `in-package` clause:

```lisp
(in-package #:ecs-tutorial-2)


;; don't forget to remove this from map.lisp
(ecs:defcomponent (image :finalize (lambda (entity &key bitmap)
                                     (when (has-map-tile-prefab-p entity)
                                       (al:destroy-bitmap bitmap))))
  (bitmap (cffi:null-pointer) :type cffi:foreign-pointer))

(ecs:defsystem render-images
  (:components-ro (position image)
   :initially (al:hold-bitmap-drawing t)
   :finally (al:hold-bitmap-drawing nil))
  (al:draw-bitmap image-bitmap position-x position-y 0))

(defun load-bitmap (filename)
  (al:ensure-loaded #'al:load-bitmap (namestring filename)))
```

Now that we're done with refactoring, we can as usually proceed with adding new functionality. Before doing so, let's make sure the code still works by loading it in REPL with `(ql:quickload :ecs-tutorial-2)` and `(ecs-tutorial-2:main)`.

First, let's add the `animation-frame` component definition to `src/animation.lisp`, which will represent a single frame of an animation:

```lisp
(ecs:defcomponent animation-frame
  (sequence :|| :type keyword :index sequence-frames)
  (duration 0.0 :type single-float))
```

In addition to the `duration` slot, which specifies the frame's duration in seconds, we also add a `sequence` slot to the component allowing us to distinguish between different animations. The latter also has an index function called `sequence-frames` which will return a list of all frames in a given animation. This will help us find the next frame of animation when switching frames without having to loop through all of them. Additionally, it will be convenient later to use human-readable names when switching between different character animations, such as going from idle to running animation. Note that the type of `sequence` slot is [`keyword`](https://cl-community-spec.github.io/pages/keyword.html), a subtype of [symbols](https://cl-community-spec.github.io/pages/symbol.html) which undergo [interning](https://en.wikipedia.org/wiki/Interning_(computer_science)). This means that the same symbols are reused instead of creating new ones, ensuring that two symbols with identical string content are always literally the same object. This allows for efficient symbol (and keyword) comparison by simply comparing pointers for numerical equality, which is why we use them to identify animations.

The `animation-frame` component will be added to tile prefabs, as it is essentially a template created in a single instance for each frame of every animation. To distinguish between specific animated tiles on the map, we will need a component that stores the current animation state. We'll call it `animation-state`:

```lisp
(ecs:defcomponent animation-state
  (sequence :|| :type keyword)
  (frame 0 :type fixnum)
  (duration 0.0 :type single-float)
  (elapsed 0.0 :type single-float))
```

In this component, we store the current animation name in the `sequence` slot. Additionally, there is a `frame` slot to track the current animation frame number. Next, we'll copy the current frame's duration into the `duration` slot from the `animation-frame` component to avoid unnecessary memory hops at each iteration of the game loop. Finally, we store the elapsed time in seconds during which the current frame has been displayed on the screen in the `elapsed` slot.

Thus, the data storage architecture for animations will look like this (entities are again represented by very rough vertical rectangles and components by horizontal ones):

![](uploads/2e5cba01346248fa8abc5c80d1901373/animation-frames-state.png)

Now let's add a [`let-plus`](https://github.com/sharplispers/let-plus) library dependency to the `ecs-tutorial-2.asd` file, it provides syntactic sugar that will help us write cleaner code for switching animation frames:

```lisp
  ;; ...
  :license "MIT"
  :depends-on (#:alexandria
               #:cl-fast-ecs
               #:cl-liballegro
               #:cl-liballegro-nuklear
               #:cl-tiled
               #:let-plus               ;; here
               #:livesupport)
  ;; ...
```

...and we'll reference this library in the `src/package.lisp` file. While we're at it, we'll also import the [`make-keyword`](https://alexandria.common-lisp.dev/draft/alexandria.html#index-make_002dkeyword) function from the [alexandria](https://alexandria.common-lisp.dev) library, we'll need it later:

```lisp
(defpackage #:ecs-tutorial-2
  (:use #:cl #:let-plus)                                        ;; here
  (:import-from #:alexandria #:define-constant #:make-keyword)  ;; and here
  (:local-nicknames (#:tiled #:cl-tiled))
  (:export #:main))
```

Now, there's nothing stopping us from defining an `update-animations` system in `src/animation.lisp` that'll switch animation frames at the right moments in time:

```lisp
(ecs:defsystem update-animations
  (:components-rw (animation-state image)
   :arguments ((dt single-float)))
  (incf animation-state-elapsed dt)
  (when (> animation-state-elapsed animation-state-duration)
    (let+ (((&values nframes rest-time) (floor animation-state-elapsed
                                               animation-state-duration))
           (sequence-frames (sequence-frames animation-state-sequence))
           ((&values &ign nframe) (truncate (+ animation-state-frame nframes)
                                            (length sequence-frames)))
           (frame (nth nframe sequence-frames)))
      (setf animation-state-elapsed rest-time
            animation-state-frame nframe
            animation-state-duration (animation-frame-duration frame)
            image-bitmap (image-bitmap frame)))))
```

This system will go through all animated tiles without affecting the prefabs, as they won't have the `animation-state` component. In the system, we use the [`incf`](https://cl-community-spec.github.io/pages/incf.html) macro to increment the `elapsed` slot in the `animation-state` component by the amount of real physical time `dt` that has passed since the previous iteration of the main game loop. Then, if `elapsed` exceeds `duration`, the interesting part begins: we need to switch to the next animation frame. To do this, we use the [`floor`](https://cl-community-spec.github.io/pages/floor.html) function to divide `elapsed` by `duration`, storing the integer quotient in the `nframes` variable and the remaining fractional part in the `rest-time` variable. The idea is that due to potential lags, `dt` may be greater than `duration`, meaning we might need to switch more than one animation frame within the time passed since the previous frame. The `nframes` variable represents this number of frames, though in most cases it would be simply one. Meanwhile `rest-time` is the amount of time smaller than `duration` that the next frame should have been displayed already. Next, we retrieve the list of all frames in the current animation into the `sequence-frames` variable by calling the index function `sequence-frames`. We then calculate the next frame number by adding the current one, `animation-state-frame`, to the previously calculated `nframes` (which, again, is usually one) and use the [`truncate`](https://cl-community-spec.github.io/pages/floor.html) function to do Euclidean division of the sum by the length of the `sequence-frames` list. This ensures we don't exceed the frame list's length, allowing us to loop back to the start of animation if we're currently displaying the last frame. The result of this operation is stored in the `nframe` variable, representing the next frame number. We then extract the prefab entity for the next frame using the [`nth`](https://cl-community-spec.github.io/pages/nth.html) function and store it in the `frame` variable. Finally, we update using [`setf`](https://cl-community-spec.github.io/pages/setf.html) the following:

* `elapsed` is set to `rest-time`, the leftover time from the previous frame, during which the new current frame should have been displayed;
* `frame` is updated to the new frame number `nframe`;
* `duration` is copied from the `duration` slot of the `animation-frame` component of prefab object `frame` obtained by the call to `animation-frame-duration` accessor;
* the `bitmap` slot in the `image` component is updated with the data from the same `frame` prefab, effectively changing the displayed image.

Now we need to modify the map loading process to add the `animation-frame` component to prefabs when necessary, as well as to add the `animation-state` component to animated tiles on the map. We'll need several helper functions for this, so let's add the following to `src/animation.lisp`:

```lisp
(defun animation->spec (frame sequence)
  (copy-list
   `((:animation-frame :sequence ,sequence
                       :duration ,(* 0.001 (tiled:frame-duration frame))))))

(defun instantiate-animation (entity prefab)
  (let ((duration (animation-frame-duration prefab)))
    (make-animation-state entity
                          :sequence (animation-frame-sequence prefab)
                          :duration duration
                          :elapsed (random duration))))
```

The `animation->spec` function will return a specification fragment for the [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT) function with the `animation-frame` component data, particularly with the correctly calculated `duration` (in Tiled, animation durations are stored in milliseconds).

The `instantiate-animation` function will be called when creating animated tiles on the map to add the `animation-state` component with appropriate initial values: the `sequence` slot will copy the animation sequence name from the prefab, the `duration` slot will copy the frame's duration, and the `elapsed` slot will contain a [random](https://cl-community-spec.github.io/pages/random.html) value between 0 and `duration`, so that the same animations on the map don't play in perfect sync, which would look strange.

Now let's modify the `load-tile-prefab` function in `src/map.lisp` as follows:

```lisp
(defun load-tile-prefab (tile bitmap map-entity &optional extra-specs)        ;; here
  (unless (ecs:entity-valid-p
           (map-tile-prefab (tiled:tile-gid tile) :missing-error-p nil))
    (let* ((internal-tile-spec (tile->spec tile bitmap map-entity))
           (properties (when (typep tile 'tiled:properties-mixin)                ;; here
                         (tiled:properties tile)))                               ;;
           sequence frames                                                       ;;
           (animation-spec                                                       ;;
             (when (typep tile 'tiled:animated-tile)                             ;;
               (setf sequence (make-keyword                                      ;;
                               (string-upcase (gethash "sequence" properties)))  ;;
                     frames (tiled:tile-frames tile))                            ;;
               (animation->spec (first frames) sequence)))                       ;;
           (tile-spec (ecs:spec-adjoin                                           ;;
                       (nconc internal-tile-spec animation-spec extra-specs)     ;;
                       '(:map-tile)))                                            ;;
           (tile (ecs:make-object tile-spec)))
      (dolist (frame (rest frames))                                              ;; and here
        (load-tile-prefab (tiled:frame-tile frame) bitmap map-entity             ;;
                          (animation->spec frame sequence)))                     ;;
      tile)))
```

The animated tile object returned by the `cl-tiled` library, `tiled:animated-tile`, is structured in such a peculiar way that the `tile-frames` slot contains a list of all animation frames, with the first frame being the `animated-tile` itself. We take this into account by setting the `animation-spec` variable with the specification returned by `animation->spec`, and also assigning the animation name to the `sequence` variable and the list of frames to the `frames` variable. If the tile passed in the `tile` parameter is not animated, these variables will be set to [`nil`](https://cl-community-spec.github.io/pages/NILv.html). We add the `animation-spec` to the specification corresponding to tile's inherent components (such as `image` and `size`), which are stored in the `internal-tile-spec` variable, by calling the [`nconc`](https://cl-community-spec.github.io/pages/nconc.html) function (it modifies the provided lists and thus works a bit faster than its immutable version [`append`](https://cl-community-spec.github.io/pages/append.html)). Additionally, we load all animation frames by recursively calling `load-tile-prefab`, passing the result of `animation->spec` for each frame as the new optional `extra-specs` parameter, which is also passed to the `nconc` function to form the final prefab object specification.

Here again we rely on the implicit order of created entities; the frames returned by `tiled:tile-frames` are in the same order as they appear in the animation editor, and the entities returned by the ECS framework index functions are always sorted by entity number. Thus, we will always retrieve animation frames from the index in the correct order.

Not that we obtain the animation name from the tile's property named `"sequence"`, so we will need to add it in the tileset editor by right-clicking in the properties editor, selecting *Add Property*, and then entering *sequence* as the name and *string* as the type in the popup dialog, for instance, for the burning flame with the sequence called *flame-idle*:

![](uploads/13650c0165877832d607d9c8a2149642/animation-sequence-en.png)

If we skip this important step, we will get errors like `The value NIL is not of type FIXNUM` or similar. This happens because animations will be loaded under the name `NIL`, which is returned by [`gethash`](https://cl-community-spec.github.io/pages/gethash.html) for an empty hash table, and we won't be able to find them by the names we expect.

Finally, we modify the `load-tile` function to initialize animations from prefabs when needed, i.e. when they contain animation data:

```lisp
(defun load-tile (entity tile x y)
  (let ((prefab (map-tile-prefab (tiled:tile-gid tile))))
    (ecs:copy-entity prefab
                     :destination entity
                     :except '(:map-tile-prefab :animation-frame))  ;; here
    (when (has-animation-frame-p prefab)                            ;;
      (instantiate-animation entity prefab))                        ;;
    (make-position entity :x (float x)
                          :y (float y))))
```

We specify the `animation-frame` component in the exception list of the [`copy-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ACOPY-ENTITY) function (the `except` keyword argument) when copying prefab data. Next, if the prefab contains animation (we check this by calling the framework-generated predicate `has-animation-frame-p`), we call our helper function `instantiate-animation` to properly create the animation component in the tile entity.

Finally, we can reload all the accumulated changes in the system by calling `(ql:quickload :ecs-tutorial-2)` and run `(ecs-tutorial-2:main)` to enjoy the sight of a dungeon coming to life:

![](uploads/9a924018feadfb8ccf13d5e83536252b/animation.gif)

### Character

Now it's time to add the player's virtual alter ego to the game, a character that will be controlled using the keyboard. Let's create a new file in our project, `src/character.lisp`, and reference it in `ecs-tutorial-2.asd`:

```lisp
  ;; ...
  :components ((:module "src"
              :components
              ((:file "package")
               (:file "common")
               (:file "animation")
               (:file "map")
               (:file "character")  ;; here
               (:file "main"))))
  ;; ...
```

We'll also update `src/package.lisp` by adding an `:import-from` directive to import a few symbols from the [`float-features`](https://shinmera.github.io/float-features) library, which we will need shortly (`float-features` is a dependency of libraries we use, so we don't need to explicitly add it to `:depends-on`):

```lisp
(defpackage #:ecs-tutorial-2
  (:use #:cl #:let-plus)
  (:import-from #:alexandria #:define-constant #:make-keyword)
  (:import-from #:float-features #:single-float-nan #:float-nan-p)  ;; here
  (:local-nicknames (#:tiled #:cl-tiled))
  (:export #:main))
```

Let's load these structural changes in our project by running `(ql:quickload :ecs-tutorial-2)`. Then we'll add the regular header with project package activation along with the definition for the character component:

```lisp
(in-package #:ecs-tutorial-2)


(ecs:defcomponent character
  (speed 0.0 :type single-float)
  (target-x single-float-nan :type single-float)
  (target-y single-float-nan :type single-float))
```

The main difference between characters and other objects we display on the screen is that characters can move. Therefore, important data for them is speed (in pixels per second) and the point they are currently moving toward. For the initial target point, we use [`single-float-nan`](https://shinmera.github.io/float-features/#VARIABLE%20FLOAT-FEATURES%3ASINGLE-FLOAT-NAN) from the `float-features` library, which is essentially the special value of IEEE-754 floating-point, `NaN` ("Not a Number") also found in other programming languages. This way we signal that the character isn't heading anywhere initially. If we had used zero for those values, all newly created characters would for no reason immediately start running toward the top-left corner of the map.

Next, one of the characters will be special, as they will be controlled by the player via the keyboard, let's make a tag component for it:

```lisp
(ecs:defcomponent player
  (player 1 :type bit :index player-entity :unique t))
```

Here we use a trick by adding a similarly named `player` slot of type `bit` (an integer that can only have two values, 0 or 1) to the otherwise empty tag component. We also assign a unique index named `player-entity` to it so that we can retrieve the player entity with an *O(1)* lookup by calling `(player-entity 1)` instead of storing it in an ugly global variable. Although this elegant solution (impaired by virtually irrelevant number 1) introduces a slight performance trade-off — the lookup will take at least six memory references instead of just one or two — it's still efficient.

Note that the flexibility of the ECS architecture allows us to dynamically transfer control to any character at any time. We can simply remove the `player` component from the current character and add it to another one, thus getting the opportunity to create a postmodern game plot twist 🤯

Our main character will be this adorable, toothy orc from the tileset:

![](uploads/3ce86f15c7ff73d90af68dd5f8d4636e/player.png)

We can crop the section of the image that contains the orc by running the following command in the `Resources` directory:

```sh
convert dungeontileset-extended2.png -crop 32x32+736+64 player.png
```

Now, we'll write a `load-player` function in `src/character.lisp` to load this image and create the player character object using [`ecs:make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT):

```lisp
(defun load-player ()
  (ecs:make-object `((:image :bitmap ,(load-bitmap "player.png"))
                     (:position :x 64.0 :y 64.0)
                     (:size :width 32.0 :height 32.0)
                     (:character :speed 100.0)
                     (:player))))
```

We'll also call it in the `init` function from `src/main.lisp` after the `load-map` call, so that the player character's image is drawn above the map tiles due to implicit entity order we discussed earlier:

```lisp
(defun init ()
  (ecs:bind-storage)
  (load-map "level1.tmx")  ;;
  (load-player))           ;; here
```

To send the definitions of the `character` and `player` components, as well as the `load-player` and `init` functions, to the running Lisp process, place the cursor on each of those and press Ctrl-C, Ctrl-C (`C-c C-c` in Emacs jargon), or use the relevant context menu options like "*Inline eval*", "*Evaluate This S-expression*", "*Evaluate form at cursor*", or similar in your IDE. Reloading the entire system by calling `(ql:quickload :ecs-tutorial-2)` in REPL is another option, but loading a few functions and component definitions is much faster than a full reload, and the interactivity and the snappiness of the "changes 🡒 result" loop are very important when developing games. After running `(ecs-tutorial-2:main)` in REPL, we'll see our character:

![](uploads/4e712e2f66f638914bcb781f95483d73/static-player.png)

However, we haven't implemented controls for this character yet, let's fix that. We'll start by adding a `move-character` system to the `src/character.lisp` file. This system will help characters to move and reach their target, which is stored in the `target-x` and `target-y` slots:

```lisp
(defun approx-equal (a b &optional (epsilon 0.5))
  (< (abs (- a b)) epsilon))

(ecs:defsystem move-characters
  (:components-rw (position character)
   :components-ro (size)
   :arguments ((dt single-float)))
  (when (or (float-nan-p character-target-x)
            (float-nan-p character-target-y))
    (setf character-target-x position-x
          character-target-y position-y))
  (unless (and (approx-equal position-x character-target-x)
               (approx-equal position-y character-target-y))
    (let* ((angle (atan (- character-target-y position-y)
                        (- character-target-x position-x)))
           (dx (* character-speed dt (cos angle)))
           (dy (* character-speed dt (sin angle)))
           (new-x (+ position-x dx))
           (new-y (+ position-y dy)))
      (setf position-x new-x
            position-y new-y))))
```

First, we implement a helper function for coordinate comparison, `approx-equal`, since directly comparing floating-point numbers is a [big sin](https://0.30000000000000004.com). In the `move-characters` system, we check whether the character is newly created with `target-x` and `target-y` equal to `NaN`, using the [`float-nan-p`](https://shinmera.github.io/float-features/#FUNCTION%20FLOAT-FEATURES%3AFLOAT-NAN-P) from `float-features`. If so, we set `target-x` and `target-y` to the character's current coordinates, so their target becomes the location they are already at — somewhat like a Zen koan 🤭 Next, we use `approx-equal` to check if the character has reached their target point. If not, we calculate the new `x` and `y` coordinates, `new-x` and `new-y`, based on current position, the target coordinates, the character's movement speed, the elapsed time `dt` since the last frame, and some basic geometry:

![](uploads/d22884086e88dc56e5257740db166b43/movement.png)

We then set the character's current coordinates, `position-x` and `position-y`, to the calculated values.

Next, let's write a system that polls the keyboard and moves the player character according to the keys pressed. First, we need to import the [`clamp`](https://alexandria.common-lisp.dev/draft/alexandria.html#index-clamp) function from the `alexandria` library in `src/package.lisp`:

```lisp
(defpackage #:ecs-tutorial-2
  (:use #:cl #:let-plus)
  (:import-from #:alexandria #:clamp #:define-constant #:make-keyword)  ;; here
  (:import-from #:float-features #:single-float-nan #:float-nan-p)
  (:local-nicknames (#:tiled #:cl-tiled))
  (:export #:main))
```

Then, add the `control-player` system definition to `src/character.lisp` after the `move-characters` system:

```lisp
(ecs:defsystem control-player
  (:components-ro (player position size)
   :components-rw (character)
   :after (move-characters))
  (al:with-current-keyboard-state keyboard-state
    (let ((dx 0) (dy 0))
      (when (al:key-down keyboard-state :W) (setf dy -1))
      (when (al:key-down keyboard-state :A) (setf dx -1))
      (when (al:key-down keyboard-state :S) (setf dy +1))
      (when (al:key-down keyboard-state :D) (setf dx +1))

      (setf
       character-target-x
       (clamp (+ position-x dx) size-width (- +window-width+ size-width))
       character-target-y
       (clamp (+ position-y dy) size-height (- +window-height+ size-height))))))
```

By specifying `:after (move-characters)`, we ensure that this system runs after `move-characters`, preventing issues with `NaN` values in the `character-target-x` and `character-target-y` slots. The system uses the [`with-current-keyboard-state`](https://github.com/resttime/cl-liballegro/blob/eea31c9c949c1003d290db3f96564d9bffee0b8f/src/types/keyboard.lisp#L13) macro from [`cl-liballegro`](https://github.com/resttime/cl-liballegro). This macro allocates a C structure [`ALLEGRO_KEYBOARD_STATE`](https://liballeg.org/a5docs/trunk/keyboard.html#allegro_keyboard_state) on the stack, fills it with the current keyboard state using a call to [`al_get_keyboard_state`](https://liballeg.org/a5docs/trunk/keyboard.html#al_get_keyboard_state) and allows us to work with it using a pointer. We do that by calling [`al_key_down`](https://liballeg.org/a5docs/trunk/keyboard.html#al_key_down) to check presses for keys *W*, *A*, *S* and *D*, changing `dx` and `dy` variables accordingly. We then add these variables to the current coordinates, `position-x` and `position-y`, and update them with [`setf`](https://cl-community-spec.github.io/pages/setf.html), using the [`clamp`](https://alexandria.common-lisp.dev/draft/alexandria.html#index-clamp) function to keep the character within screen bounds, namely:

* the new `x` coordinate cannot be smaller than `size-width` or larger than the screen width minus `size-width`,
* the new `y` coordinate cannot be smaller than `size-height` or larger than the screen height minus `size-height`.

Finally, send the `defpackage` form from `src/package.lisp`, as well as `approx-equal` function and the new `move-characters` and `control-player` systems, to the running Lisp process using `C-c C-c` (or your IDE's equivalent). Without even restarting `main`, you'll see that the controls are working 🎉

[![](uploads/2c14b0a04fe271935940f936f5333f44/0.jpg)](https://youtu.be/7RfdfsKfWxU)

However, you can notice an oversight — our character runs freely through objects, just like T-1000 terminator passing through prison bars.

![](uploads/ce40e9c8ebe524bc57431924bdc8e87d/terminator.jpg)

This happens because currently, the walls are just regular images, no different from the floor tiles. Now is the time to load more than just pictures from Tiled.

### Interior

The Tiled editor is powerful and flexible enough to store arbitrary objects with properties of various types in a map file. Naturally, we won't pass by this flexibility and will exploit it to load ECS objects directly from Tiled, bringing us closer to full-fledged game engines with built-in editors like Unreal or Godot. Plus, we won't have to hardcode the player's parameters, as we did in the `load-player` function. Instead, we can create the player directly in the editor and place them where needed along with other characters.

Let's begin by solving the issue of the character passing through walls. There already is a Boolean `obstacle` slot in the `map-tile` component, intended to distinguish obstacles from other decor, but we haven't used it yet. We need to do two things: first, load ECS components, including `map-tile`, from the map file, and second, use the loaded `obstacle` slot values.

We'll start by adding components to the map so we have something to load. First, we'll create a [custom Tiled type](https://doc.mapeditor.org/en/stable/manual/custom-properties/#custom-types) corresponding to our `map-tile` component. To do this, go to *View 🡒 Custom Types Editor*, click the *Add Class* button in the dialog that opens, and name it `map-tile`. Then click *Add Member*, choose `bool` as the type, and name it `obstacle`:

![](uploads/7a90e383d661e6360a7ea3192a1e7aac/map-tile-type-en.png)

The thing is that we can now use this type as a property type for tiles: by opening the tileset editor (using *Edit Tileset* gear button in the *Tilesets* window), selecting a wall tile, right-clicking in the *Properties* window, and choosing *Add Property*, then selecting our new `map-tile` type from the list, and naming the new property the same, *map-tile* (this is important both for clarity and for the correct loading of the property by our code). Also, tick the `obstacle` checkbox, since a wall is indeed an obstacle. It will look like this:

![](uploads/cc3617a976a1ce429825901fe77eeb12/map-tile-property-en.png)

Now we need to load the custom tile properties as components. To do this, let's first add a new helper function `properties->spec` and modify the `load-tile-prefab` function in the `src/map.lisp` file:

```lisp
(defun properties->spec (properties)
  (when properties
    (loop :for component :being :the :hash-key
          :using (hash-value slots) :of properties
          :when (typep slots 'hash-table)
          :collect (list* (make-keyword (string-upcase component))
                          (loop :for name :being :the :hash-key
                                :using (hash-value value) :of slots
                                :nconcing (list
                                           (make-keyword (string-upcase name))
                                           value))))))

(defun load-tile-prefab (tile bitmap map-entity &optional extra-specs)
  (unless (ecs:entity-valid-p
           (map-tile-prefab (tiled:tile-gid tile) :missing-error-p nil))
    (let* ((internal-tile-spec (tile->spec tile bitmap map-entity))
           (properties (when (typep tile 'tiled:properties-mixin)
                         (tiled:properties tile)))
           (external-tile-spec (properties->spec properties))                 ;; here
           sequence frames
           (animation-spec
             (when (typep tile 'tiled:animated-tile)
               (setf sequence (make-keyword
                               (string-upcase (gethash "sequence" properties)))
                     frames (tiled:tile-frames tile))
               (animation->spec (first frames) sequence)))
           (tile-spec (ecs:spec-adjoin
                       (nconc internal-tile-spec external-tile-spec           ;; here
                              animation-spec extra-specs)                     ;;
                       '(:map-tile)))
           (tile (ecs:make-object tile-spec)))
      (dolist (frame (rest frames))
        (load-tile-prefab (tiled:frame-tile frame) bitmap map-entity
                          (animation->spec frame sequence)))
      tile)))
```

The `properties->spec` function takes a hash table (which `cl-tiled` uses to represent object properties) as input and returns a corresponding specification list for the [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT) function. Custom classes defined in Tiled will be treated as components, and their members will be component slots. For example, for the wall tile property `mpa-tile` we've added earlier, this function will return a list like `((:map-tile :obstacle t))` — exactly what we need.

The `load-tile-prefab` function now calls the `properties->spec` helper function for the tile properties obtained by the call to the `tiled:properties` function. If the tile doesn't inherit from the CLOS class `tiled:properties-mixin` (meaning it has no properties, which would often happen), the `properties` variable will contain [`nil`](https://cl-community-spec.github.io/pages/NILv.html) (this is how the standard [`when`](https://cl-community-spec.github.io/pages/when.html) macro works), and `properties->spec` will also return an empty list as the specification. In addition, the loaded tile properties specification, stored in the `external-tile-spec` variable, is now appended to the final prefab specification, which is assembled from parts by the familiar [`nconc`](https://cl-community-spec.github.io/pages/nconc.html) function. Now the call to [`spec-adjoin`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ASPEC-ADJOIN) makes much more sense: if the tile properties in the tileset do not include the `map-tile` property (which would be the case more often than not), the prefab specification will include the `map-tile` component with the default `obstacle` slot value of [`nil`](https://cl-community-spec.github.io/pages/NILv.html) (no obstacle). However, if it was loaded from properties into the `external-tile-spec` variable, `spec-adjoin` will leave the specification unchanged.

Having sent the definitions of the `properties->spec` and `load-tile-prefab` functions by means of your IDE and run in REPL `(ecs-tutorial-2:main)`, you may wonder how to verify that the mechanism we've implemented works, aside from the obvious absence of errors during startup. To do that, we can use the tools we already have at hand:

1. The `map-tile-prefab` component contains a corresponding index.
2. The ECS framework includes the function [`print-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3APRINT-ENTITY), which displays the components present in the given entity in the same specification format that we feed into [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT).

Having remembered the tile *ID* from the previous Tiled screenshot (this is the unique numeric identifier of the tile), we can inspect the prefab loaded from it by running the following code snippet in the REPL while the program is running: `(ecs:print-entity (ecs-tutorial-2::map-tile-prefab (1+ 65)))`, where 65 is the tile ID's value. As a result, you'll see something like this:

```lisp
((:MAP-TILE :OBSTACLE T) (:PARENT :ENTITY 0)
 (:IMAGE :BITMAP #.(SB-SYS:INT-SAP #X5605DA7690A0)) (:MAP-TILE-PREFAB :GID 66)
 (:SIZE :WIDTH 32.0 :HEIGHT 32.0))
```

...which means that in addition to the `map-tile-prefab` component and the necessary components for the tile (`parent`, `image` and `size`), the prefab now includes a `map-tile` component with an `obstacle` slot set to [`t`](https://cl-community-spec.github.io/pages/t.html), and we know for sure that it will be copied upon loading into all corresponding tile instances on the map via the call to [`ecs:copy-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ACOPY-ENTITY)  🎉

Now that we are loading the dungeon interior data into the game, let's use this data to stop our character from walking through walls, taking away the Terminator ability to pass through obstacles. We'll take the simplest approach and modify the `control-player` system to block any such attempt. However, we will need a mechanism to get all objects located on a given tile. Once we detect an obstacle tile among them, we will prevent the character from moving onto that tile. For this, we can once again rely on the `cl-fast-ecs` slot indexing feature, to avoid iterating over all objects on the map with *O(n)* complexity. But which slot and which component should we index? Since the task involves object locations, it makes sense to use the `position` component. It will also make sense to make the new indexed slot to be some kind of coordinate hash, which we can pack, say, into a single integer, ensuring that different tiles in different 32⨉32 grid nodes have unique hashes. Given that our coordinates are stored as floating-point numbers but represent integers (as they refer to pixels), and fall within the ranges *[0; 1280]* for `x` and *[0; 800]* for `y`, we can write a simple hash function that converts the coordinates into integers by truncating the decimal part, and packs two integers (both guaranteed to be less than 2³²) into a single 64-bit integer. Let's add a new helper function `tile-hash` to the `src/common.lisp` file and a new indexed slot to the `position` component:

```lisp
(defun tile-hash (x y)
   (let ((x* (truncate x))
         (y* (truncate y)))
     (logior (ash x* 32) y*)))

(ecs:defcomponent position
  (x 0.0 :type single-float)
  (y 0.0 :type single-float)
  (tile-hash (tile-hash x y) :type fixnum :index tiles))  ;; here
```

The new slot is called `tile-hash` and has a default value equal to the result of the call to `tile-hash` function with `x` and `y` arguments, which refer to the corresponding slot values during component creation. It also has index function named `tiles`. Thus, by calling `tile-hash` with the coordinates of a tile's top-left corner and passing the resulting hash to the `tiles` function, we will get a list of all entities whose coordinates match top-left corner of that tile. These will be the map tiles from all layers in the grid node we are interested in.

Now let's modify the `control-player` system in `src/character.lisp` using three new helper functions:

```lisp
(defun tile-start (x y width height)
  (values (* width (the fixnum (floor x width)))
          (* height (the fixnum (floor y height)))))

(defun tile-obstacle-p (x y)
  (loop :for entity :of-type ecs:entity :in (tiles (tile-hash x y))
        :thereis (and (has-map-tile-p entity)
                      (map-tile-obstacle entity))))

(defun obstaclep (x y size-width size-height)
  (multiple-value-call #'tile-obstacle-p
    (tile-start x y size-width size-height)))

(ecs:defsystem control-player
  (:components-ro (player position size)
   :components-rw (character)
   :after (move-characters))
  (al:with-current-keyboard-state keyboard-state
    (let ((dx 0) (dy 0))
      (when (al:key-down keyboard-state :W) (setf dy -1))
      (when (al:key-down keyboard-state :A) (setf dx -1))
      (when (al:key-down keyboard-state :S) (setf dy +1))
      (when (al:key-down keyboard-state :D) (setf dx +1))

      (setf
       character-target-x
       (clamp (+ position-x dx) size-width (- +window-width+ size-width))
       character-target-y
       (clamp (+ position-y dy) size-height (- +window-height+ size-height)))

       (when (or (and (or (minusp dx) (minusp dy))                             ;; here
                     (obstaclep character-target-x                             ;;
                                character-target-y                             ;;
                                size-width size-height))                       ;;
                (and (or (minusp dx) (plusp dy))                               ;;
                     (obstaclep character-target-x                             ;;
                                (+ character-target-y size-height)             ;;
                                size-width size-height))                       ;;
                (and (or (plusp dx) (minusp dy))                               ;;
                     (obstaclep (+ character-target-x size-width)              ;;
                                character-target-y                             ;;
                                size-width size-height))                       ;;
                (and (or (plusp dx) (plusp dy))                                ;;
                     (obstaclep (+ character-target-x size-width)              ;;
                                (+ character-target-y size-height)             ;;
                                size-width size-height)))                      ;;
        (setf character-target-x position-x                                    ;;
              character-target-y position-y)))))                               ;;
```

The `tile-start` function, for given coordinates and grid tile sizes, returns the coordinates of the top-left corner of the tile containing this point. It does so by simply dividing the coordinates by the tile size using [`floor`](https://cl-community-spec.github.io/pages/floor.html) and multiplying the resulting integer of type [`fixnum`](https://cl-community-spec.github.io/pages/fixnum.html) back:

![](uploads/3a67311350a85a25c7e080e8b9272f6b/tile-start.png)

(the `//` symbol in the illustration denotes integer division). `tile-start` returns its results using the [multiple values](https://cl-community-spec.github.io/pages/values.html) Lisp mechanism, which allows returning more than one value from a function without allocating additional memory on the heap, as it happens, for example, in Python when using tuple unpacking.

The `tile-obstacle-p`, for given tile coordinates, returns a Boolean value indicating whether there is at least one obstacle tile at these coordinates. It goes through all entities whose `tile-hash` matches the hash of the given `x` and `y` coordinates with the standard [`loop`](https://cl-community-spec.github.io/pages/loop.html) construct, using the index function `tiles` to get the list. As soon as it finds an entity that meets the condition `(and (has-map-tile-p entity) (map-tile-obstacle entity))`, meaning it hash a `map-tile` component and its `obstacle` slot is true, it immediately returns [`t`](https://cl-community-spec.github.io/pages/t.html), using [`thereis`](https://cl-community-spec.github.io/pages/Summary-of-Termination-Test-Clauses.html) clause of the `loop` macro. If the loop reaches the end without finding such an entity, it automatically returns false, [`nil`](https://cl-community-spec.github.io/pages/NILv.html). This seemingly non-trivial code, not to mention the additional index, for what appears to be a simple task of detecting obstacles, is necessary because the map may have multiple layers, and different tiles may be located in the same grid node, so we need to check them all.

The `obstaclep` function combines the previous two: it checks whether a tile is an obstacle at any arbitrary coordinate (`x`, `y`), not just those exactly at the grid nodes, by calling `tile-obstacle-p` with the results of the `tile-start` function using the [`multiple-value-call`](https://cl-community-spec.github.io/pages/multiple_002dvalue_002dcall.html) special form.

Finally, in the `control-player` system, after determining the direction in which the player wants to move their character, stored in `dx` and `dy`, we add a new block of code. The new code ensures that the character can move to the coordinates the player wants by checking whether the corresponding tiles are passable, again following straightforward geometric considerations:

![](uploads/0fa74efcc82d661e1dedebe54675be4f/tile-obstacles.png)

We check the shaded tiles in the illustration when moving in each of the four directions to see if they are obstacles — this is exactly what the bulky condition in [`when`](https://cl-community-spec.github.io/pages/when.html) form does.

After sending the `tile-hash` function and the new `position` component definition to the Lisp process, as well as the `tile-start`, `tile-obstacle-p`, and `obstaclep` functions along with the updated `control-player` definition, we can see that the player's character now behaves as a proper character should — they respectfully stop before walls and no longer try to pass through them 😄

It's worth noting that the obstacle detection method we've implemented isn't perfect — it's somewhat bulky and will cause minor oddities when implementing behavior for other characters later. A better approach would be to use the coordinates of the character's tile center as their position, which would simplify the math and the code in `control-player`. However, this would require changes to the `load-map` function and the implementation of a separate system in addition to `render-images` for rendering characters' tiles at the correct coordinates. We won't take this path to avoid complicating the tutorial, but now you have the opportunity to learn from this mistake and implement a more accurate obstacle detection method in your own projects.

### Character Animation

Now let's use the ability to load components from map file, which we implemented in previous section, so that we no longer hardcode the player's parameters but set them directly in the editor. For this, we'll need new custom classes in Tiled for the character and player components. Add them by invoking the custom types editor from the *View* menu:

![](uploads/7b311c9e83625b530dd9a78f95b9a235/map-character-type-en.png)

In the `character` class, we add a single member of type `float` called `speed`. We omit the `target-x` and `target-y` slots, so they will always be initialized with their default values when loaded from map.

![](uploads/f6585703b8f345f44188331124c7c5b6/map-player-type-en.png)

In the `player` class, we add a member with the same name of type `int` and default value of `1`.

Now we can add the player's character to the map. First, in *Layers* window, create a new object layer by right-clicking and choosing *New 🡒 Object Layer* and then select it. Then, in the toolbar, enable the *Insert Tile* tool (or simply press the T key), and in the *Tilesets* window, choose the tile representing our character. Now you can click anywhere on the map, and in that location, a special object will be added to the object layer — on one hand, it can have custom type properties that will be loaded as ECS components by our code, and on the other hand, it will be treated as a tile and rendered as such. Just make sure the object layer is above all other layers so that player character and other objects aren't overwritten by map tiles during rendering. Generally speaking, the object layer can be overlapped by others if dictated by the artistic vision, but in our simple case, it's always on top.

Let's add the components we're interested in, `character` and `player` to this object using the *Properties* window (remember, it's important that the names of custom properties match the names of components):

![](uploads/fb0946183c641da85f6f25898a187940/player-properties-en.png)

Now in order for this special object to be loaded by our code, let's modify the `load-map` function in `src/map.lisp`:

```lisp
(defun load-map (filename)
  (let ((map (tiled:load-map filename))
        (map-entity (ecs:make-object '((:map)))))
    (dolist (tileset (tiled:map-tilesets map))
      (let ((bitmap (load-bitmap
                     (tiled:image-source (tiled:tileset-image tileset)))))
        (dolist (tile (tiled:tileset-tiles tileset))
          (load-tile-prefab tile bitmap map-entity))))
    (dolist (layer (tiled:map-layers map))
      (typecase layer
        (tiled:tile-layer
         (dolist (cell (tiled:layer-cells layer))
           (load-tile (ecs:make-entity) (tiled:cell-tile cell)
                      (tiled:cell-x cell) (tiled:cell-y cell))))
        (tiled:object-layer                                                       ;; here
         (dolist (object (tiled:object-group-objects layer))                      ;;
           (let ((object-entity (ecs:make-object                                  ;;
                                 (properties->spec (tiled:properties object)))))  ;;
             (typecase object                                                     ;;
               (tiled:tile-object                                                 ;;
                 (load-tile object-entity (tiled:object-tile object)              ;;
                            (tiled:object-x object)                               ;;
                            (- (tiled:object-y object)                            ;;
                               (tiled:object-height object))))))))))              ;;
    map-entity))
```

In this new code fragment, which runs for layers of type `tiled:object-layer`, e.g. object layers, we iterate through their contents — the list of objects obtained by calling `tiled:object-group-objects`. For each object, we create an ECS entity by calling [`make-object`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3AMAKE-OBJECT), passing the result of our helper function `properties->spec` as specification. The latter function accounts for the components we added as custom object properties in Tiled. We then check the object type — at this point, we're only interested in `tiled:tile-object` type, the tile object. For those, we call our old familiar, `load-tile`, which copies the tile's data from the prefab to the new entity, including any animation, if present, and sets the position we specified. The extra `y` calculations are needed because, for some weird reason, Tiled considers the coordinate of an object to be its bottom-left corner, not the top-left, as is the case for tiles.

Now let's remove the call to `load-player` from `init` in `src/main.lisp`:

```lisp
(defun init ()
  (ecs:bind-storage)
  (load-map "level1.tmx"))  ;; here
```

...and remove the `load-player` function from `src/character.lisp` as it is no longer needed. That's all we need to do — we effectively reuse the `properties->spec` function, which previously loaded component data from properties of tiles in the tileset into prefabs, to load it from the properties of objects directly added to the corresponding layer in Tiled. So, after sending the new function definitions for `init` and `load-map` to the Lisp process and running `(ecs-tutorial-2:main)`, we will see our character exactly where we placed them on the map 😊

Thus, by using ECS to store information about objects on the map, we are approaching the [data-driven programming](https://en.wikipedia.org/wiki/Data-driven_programming) architecture — this is an entire paradigm with its own benefits and features, often used in AAA game development. There's an excellent talk by well-known (in certain circles) game developer Mike Acton, offering a detailed overview of this paradigm: [CppCon 2014: Mike Acton "Data-Oriented Design and C++"](https://youtu.be/rX0ItVEVjHc).

Let's add a bit more flair by creating animations for our character. Our tileset is organized so that for most of the characters the first four frames of animation represent the idle animation, and the next four frames are for the running animation. We'll open the tileset editor in Tiled and make the necessary changes, ensuring we don't forget the string property *sequence* in the first frame of each animation (we'll name those `orc-idle` and `orc-run`):

![](uploads/7870e9bd88e96c5f9f0998018bfa8748/orc-idle-en.png)

![](uploads/4996344a6979730e89edd5aaaf40c6de/orc-run-en.png)

Now, let's add a helper function in the `src/animation.lisp` file that will allow us to switch animations for objects:

```lisp
(defun change-animation-sequence (entity new-sequence)
  (unless (eq (animation-state-sequence entity) new-sequence)
    (let ((first-frame (first (sequence-frames new-sequence :count 1))))
      (with-animation-state () entity
        (setf sequence new-sequence
              frame 0
              duration (animation-frame-duration first-frame)
              elapsed 0.0
              (image-bitmap entity) (image-bitmap first-frame))))))
```

In this function, we first check if the specified entity already has the requested `new-sequence` animation. We compare it with the current animation in the `sequence` slot of the `animation-state` component using the [`eq`](https://cl-community-spec.github.io/pages/eq.html), which literally checks if the memory addresses of the given objects match. Since we store animation names as keywords, which are [interned](https://en.wikipedia.org/wiki/Interning_(computer_science)), this works perfectly. If the `new-sequence` animation is indeed new, and we need to switch to it, we retrieve the first frame of the new animation by calling the `sequence-frames` index function with the keyword argument `count` set to `1` (to avoid unnecessary memory and CPU cycles spent retrieving other frames) and using [`first`](https://cl-community-spec.github.io/pages/first.html) on its result. Then, we update the slots of the entity's `animation-state` component, using the `with-animation-state` macro generated by ECS framework and the standard [`setf`](https://cl-community-spec.github.io/pages/setf.html) macro:

* The `sequence` slot is set to the new animation, `new-sequence`.
* The `frame` slot, representing the current frame number, is set to 0.
* The `duration` slot is set to the duration of the `first-frame`, retrieved via the `animation-frame-duration` accessor.
* The `elapsed` slot, tracking how long the animation has been playing, is set to 0.
* Additionally, the `bitmap` slot of the entity's `image` component is set to the same slot from the first frame entity.

We'll use this function in the `move-characters` system as follows:

```lisp
(ecs:defsystem move-characters
  (:components-rw (position character)
   :components-ro (size)
   :arguments ((dt single-float)))
  (when (or (float-nan-p character-target-x)
            (float-nan-p character-target-y))
    (setf character-target-x position-x
          character-target-y position-y))
  (if (and (approx-equal position-x character-target-x)        ;; here
           (approx-equal position-y character-target-y))       ;;
      (change-animation-sequence entity :orc-idle)             ;;
      (let* ((angle (atan (- character-target-y position-y)
                          (- character-target-x position-x)))
             (dx (* character-speed dt (cos angle)))
             (dy (* character-speed dt (sin angle)))
             (new-x (+ position-x dx))
             (new-y (+ position-y dy)))
        (setf position-x new-x
              position-y new-y)
        (change-animation-sequence entity :orc-run))))         ;; and here
```

When the character is standing still, we call `change-animation-sequence` to switch the animation to `:orc-idle`. If the character is moving, we change it to `:orc-run`. After sending the definitions of the `change-animation-sequence` function and the `move-characters` system to the Lisp process, we'll see that our character is now properly animated.

[![](uploads/c1f3a757f04c6092219f26385725daee/0.jpg)](https://youtu.be/vVB_9P4Fao0)

The fact that we've added new functionality to our character with relative ease once again highlights the flexibility of the ECS approach and the coherence of the component and system design we've chosen.

### Inhabitants

As the final step in populating our dungeon with content, let's focus on its inhabitants. We'll add an `enemy` component to the `src/character.lisp` file, which we will attach to enemy entities loaded from the map file:

```lisp
(ecs:defcomponent enemy
  (vision-range 0.0 :type single-float)
  (attack-range 0.0 :type single-float))
```

This component defines two important slots that describe enemy behavior:

* `vision-range` — stores the distance in pixels within which the enemy "sees" the player character and begins reacting to them.
* `attack-range` — stores the distance (also in pixels) within which the enemy will attack.

Next, for simplicity, we'll follow in the footsteps of [Miyazaki](https://en.wikipedia.org/wiki/Hidetaka_Miyazaki) and Souls-like games, making the enemies incredibly challenging — entering their attack range will result in instant death of the player character, game over. To implement this, we need a mechanism to actually end the game. We'll add a global variable to `src/common.lisp` to indicate when the game should end:

```lisp
(defvar *should-quit*)
```

Then, we'll modify the `main` function in `src/main.lisp` to initialize this variable and check it on every iteration of the main loop:

```lisp
               ;; ...
               :with dt :of-type double-float := 0d0
               :with *should-quit* := nil                                  ;; here
               :while (loop
                        :named event-loop
                        :while (al:get-next-event event-queue event)
                        :for type := (cffi:foreign-slot-value
                                      event '(:union al:event) 'al::type)
                        :always (not (eq type :display-close))
                        :never *should-quit*)                               ;; and here
               ;; ...
```

Now everything is ready to implement a system in `src/characters.lisp` that will manage enemy behavior:

```lisp
(ecs:defsystem handle-enemies
  (:components-ro (position enemy)
   :components-rw (character)
   :with ((player-x player-y) := (with-position () (player-entity 1)
                                   (values x y))))
  (when (and (approx-equal position-x player-x enemy-vision-range)
             (approx-equal position-y player-y enemy-vision-range))
    (setf character-target-x player-x
          character-target-y player-y))
  (when (and (approx-equal position-x player-x enemy-attack-range)
             (approx-equal position-y player-y enemy-attack-range))
    (setf *should-quit* t)
    (al:show-native-message-box (cffi:null-pointer)
                                "ECS Tutorial 2" "" "You died"
                                (cffi:null-pointer) :warn)))
```

Using the `with` argument, we introduce local variables `player-x` and `player-y`, which are initialized before entities with given components are processed with player's coordinates, obtained using the `with-position` macro generated by ECS framework. The system includes two [`when`](https://cl-community-spec.github.io/pages/when.html) forms. The first one checks if the player character is within the enemy's vision range using `approx-equal`, and if it is the case, sets the `character` component's target coordinates to the player's position, causing the enemy to "see" the player and start chasing them (if enemy's speed is lower, the player can escape by leaving their vision range). The second `when` form checks if the player is within the enemy's attack range, and if so, sets the `*should-quit*` flag to Boolean true, [`t`](https://cl-community-spec.github.io/pages/t.html), and displays a message so familiar to all Souls-like fans using the [`al_show_native_message_box`](https://liballeg.org/a5docs/trunk/native_dialog.html#al_show_native_message_box) from `liballegro`, which displays a native OS message box:

![](uploads/57ce1c3a64ac28bdb185b0680417b387/you-died.png)

Additionally, we'll modify the `move-characters` system to account for enemy animations. We'll use charming red demons as enemies, with animations named `demon-idle` and `demon-run`, but only if the entity does not have a `player` component, which we can check using the framework-generated `has-player-p` predicate:

```lisp
(ecs:defsystem move-characters
  (:components-rw (position character)
   :components-ro (size)
   :arguments ((dt single-float)))
  (when (or (float-nan-p character-target-x)
            (float-nan-p character-target-y))
    (setf character-target-x position-x
          character-target-y position-y))
  (if (and (approx-equal position-x character-target-x)
           (approx-equal position-y character-target-y))
      (change-animation-sequence                               ;; here
       entity                                                  ;;
       (if (has-player-p entity) :orc-idle :demon-idle))       ;;
      (let* ((angle (atan (- character-target-y position-y)
                          (- character-target-x position-x)))
             (dx (* character-speed dt (cos angle)))
             (dy (* character-speed dt (sin angle)))
             (new-x (+ position-x dx))
             (new-y (+ position-y dy)))
        (setf position-x new-x
              position-y new-y)
        (change-animation-sequence                             ;; and here
         entity                                                ;;
         (if (has-player-p entity) :orc-run :demon-run)))))    ;;
```

Lastly, let's add the animations *demon-idle* and *demon-run* we've referenced in the code to Tiled tileset editor:

![](uploads/09dfa3869b949e20baf54e549c2a3fd6/animation-demon-idle-en.png)

![](uploads/09d887b16cea22e2d4ce12bbd5e17faa/animation-demon-run-en.png)

...and place a few enemies on the map:

![](uploads/1e1e93f8e52606e03553a9bb46f37f1f/daemons.png)

By loading all the accumulated changes with `(ql:quickload :ecs-tutorial-2)` and launching our application with `(ecs-tutorial-2:main)`, we realize that it's no longer just a demo but a full-blown game, where players heroically overcome obstacles to reach their goal!

[![](uploads/ade92f460bc9f5d13e48313e042aa707/0.jpg)](https://youtu.be/gUQ0hBwS7kY)

However, it's easy to notice that the enemies also behave like terminators, easily passing through walls, which is too inhumane even for Souls-like games. To give the enemies some kind of artificial intelligence for navigating the dungeon terrain, we'll need a pathfinding algorithm. A common choice in games is the [A* algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm) because of its flexibility and efficiency. Fun fact: this algorithm was developed as part of a project to create a robot capable of reasoning about its actions, called [Shakey](https://en.wikipedia.org/wiki/Shakey_the_robot), and it was implemented in LISP — so by using this algorithm, we're bringing the history full circle 😌

![](uploads/c5a0bc0f039cfe76aa6de2ba84903bdc/shakey.jpg)

We could implement A* manually — it is not particularly complex — but debugging and optimizing the code might have taken a considerable amount of time. Fortunately, yours truly has already done this work and packaged it into a library called [cl-astar](https://gitlab.com/lockie/cl-astar). This library also follows the paradigm of metalinguistic abstraction, providing a set of macros to generate an optimized pathfinding function tailored to the task at hand.

So, let's integrate this library into our project by making changes to the `ecs-tutorial-2.asd` file:

```lisp
  ;; ...
  :depends-on (#:alexandria
               #:cl-astar               ;; here
               #:cl-fast-ecs
               #:cl-liballegro
               #:cl-liballegro-nuklear
               #:cl-tiled
               #:let-plus
               #:livesupport)
  ;; ...
```

...and add an import of the [`if-let`](https://alexandria.common-lisp.dev/draft/alexandria.html#index-if_002dlet) macro from `alexandria` in `src/package.lisp`, which we'll need to write a bit more elegant code:

```lisp
(defpackage #:ecs-tutorial-2
  (:use #:cl #:let-plus)
  (:import-from #:alexandria #:clamp #:define-constant #:if-let #:make-keyword)  ;; here
  (:import-from #:float-features #:single-float-nan #:float-nan-p)
  (:local-nicknames (#:tiled #:cl-tiled))
  (:export #:main))
```

Next, we need to define components to store the information about the calculated path. Here we face the question: how should we store this data? After all, it's essentially an array of points. However, if we make the array a slot in component, it would go against the ECS methodology, which the framework would kindly warn us about with a message like *values will be boxed; consider using separate entities instead*. The point is that component slots themselves are elements of large arrays in which integer entities are indices. Using an array of arrays is not exactly CPU cache friendly. The typical solution is to represent array elements as separate entities with a new component holding the necessary data. Well, let's define the components we need in `src/character.lisp`:

```lisp
(ecs:defcomponent path-point
  (x 0.0 :type single-float)
  (y 0.0 :type single-float)
  (traveller -1 :type ecs:entity :index path-points))

(ecs:defcomponent path
  (destination-x 0.0 :type single-float)
  (destination-y 0.0 :type single-float))
```

The first component, `path-point`, when added to a separate entity, represents an individual element of the array with path points. In addition to obvious `x` and `y` slots, we define an indexed slot named `traveller`, which refers to the entity that follows the path with this point. Thanks to the index, we can retrieve the full path for any given character, and since the index returns entities in strictly ascending order, the points will always be in the correct sequence — the same order in which they were initially created.

We also define the `path` component, which indicates that a character is currently following a path and stores information about its final destination. Note that we leave the similar slots in the `character` component untouched: they will indicate the nearest path point the character is moving toward, while the `path` component holds the coordinates of the ultimate destination.

Next, let's implement a system that guides a character along its path:

```lisp
(ecs:defsystem follow-path
  (:components-ro (enemy path position)
   :components-rw (character))
  "Follows path previously calculated by A* algorithm."
  (if-let (first-point (first (path-points entity :count 1)))
    (with-path-point (point-x point-y) first-point
      (if (and (approx-equal position-x point-x)
               (approx-equal position-y point-y))
          (ecs:delete-entity first-point)
          (setf character-target-x point-x
                character-target-y point-y)))
    (delete-path entity)))
```

In this system, we use the [`if-let`](https://alexandria.common-lisp.dev/draft/alexandria.html#index-if_002dlet) macro to assign the first path point, which we retrieve by calling [`first`](https://cl-community-spec.github.io/pages/first.html) on the list obtained by index function `path-points`, to the variable `path-point` and immediately check if it's [`nil`](https://cl-community-spec.github.io/pages/NILv.html). If there is at least one point in the path, i.e. `first-point` is not [`nil`](https://cl-community-spec.github.io/pages/NILv.html), we execute the code inside the `with-path-point` macro, which creates local variables `point-x` and `point-y` containing the values of the `x` and `y` slots from the `path-point` component of `first-point` entity. This code checks, using the `approx-equal` function, whether the character coordinates (`position-x`/`position-y`) are close to the point's coordinates. If so, the point has been reached, and it is deleted using the [`ecs:delete-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ADELETE-ENTITY) function, which automatically removes the point from the `path-points` index. Otherwise, the character's `target-x` and `target-y` coordinates are updated in `character` component, so it will move toward that first point on the path. Finally, if the path contains no points, `first-point` is [`nil`](https://cl-community-spec.github.io/pages/NILv.html), and the second branch of `if-let` is executed, removing the `path` component from the character using the generated `deelete-path` function. This stops the character from moving, and it will no longer be processed by this system.

Now, let's implement a pathfinding function using the `cl-astar` library:

```lisp
(a*:define-path-finder find-path (entity tile-width tile-height)
    (:variables ((world-width  (floor +window-width+  tile-width))
                 (world-height (floor +window-height+ tile-height)))
     :world-size (* world-width world-height)
     :indexer (a*:make-row-major-indexer
               world-width
               :node-width tile-width :node-height tile-height)
     :goal-reached-p (lambda (x1 y1 x2 y2)
                       (and (= (floor x1 tile-width)  (floor x2 tile-width))
                            (= (floor y1 tile-height) (floor y2 tile-height))))
     :neighbour-enumerator (lambda (x y f)
                             (let+ (((&values sx sy)
                                     (tile-start x y tile-width tile-height)))
                               (funcall (a*:make-8-directions-enumerator
                                         :node-width tile-width
                                         :node-height tile-height
                                         :max-x +window-width+
                                         :max-y +window-height+)
                                        sx sy f)))
     :exact-cost (lambda (x1 y1 x2 y2)
                   (if (or (obstaclep x2 y2 tile-width tile-height)
                           (and (/= x1 x2)
                                (/= y1 y2)
                                (or (obstaclep x1 y2 tile-width tile-height)
                                    (obstaclep x2 y1 tile-width tile-height))))
                       most-positive-single-float
                       0.0))
     :heuristic-cost (a*:make-octile-distance-heuristic)
     :path-initiator (lambda (length)
                       (declare (ignorable length))
                       (when (has-path-p entity)
                         (dolist (point (path-points entity))
                           (ecs:delete-entity point)))
                       (assign-path entity :destination-x goal-x
                                           :destination-y goal-y))
     :path-processor (lambda (x y)
                       (ecs:make-object
                        `((:path-point :x ,x :y ,y :traveller ,entity)
                          (:parent :entity ,entity))))))
```

We call the key macro from the library, [`define-path-finder`](https://lockie.gitlab.io/cl-astar/#MACRO-FUNCTION%20CL-ASTAR%3ADEFINE-PATH-FINDER), passing arguments and code fragments that tailor the pathfinding to our specific needs. First, we specify the name of the function to be defined, `find-path`, and its additional keyword arguments: `entity` (the character entity), `tile-width` and `tile-height` (since we decided to set tile sizes dynamically, we'll have to tediously pass them into all relevant functions). Next, with `variables` parameter, we define local variables for use in function code: `world-width` and `world-height` represent the screen size in tiles. We also use the `world-size` parameter to specify the total size of our entire game world in tiles, calculated as the product of `world-width` and `world-height`. Next, we define several functional parameters ([`lambda`](https://cl-community-spec.github.io/pages/lambda.html) forms or functions/macros returning other functions), which will become key parts of the `find-path` function being defined:

* `indexer`: a function that returns the integer index of a tile in an array of length `world-size` based on its coordinates. We use the predefined [`make-row-major-indexer`](https://lockie.gitlab.io/cl-astar/#MACRO-FUNCTION%20CL-ASTAR%3AMAKE-ROW-MAJOR-INDEXER) macro from the `cl-astar` library, which creates a function returning tile indices in row-major order using tile dimensions from parameters `node-width` and `node-height`.
* `goal-reached-p`: a predicate function of two pair of coordinates that returns true if the character at the first pair is considered to have reached the goal at the second. Here we simply compare tile numbers by doing integer division of the coordinates by tile size using [`floor`](https://cl-community-spec.github.io/pages/floor.html).
* `neighbour-enumerator`: a function that enumerates the neighboring tiles of the one with given coordinates, passing neighbor coordinates to given function one by one. We use the [`make-8-directions-enumerator`](https://lockie.gitlab.io/cl-astar/#MACRO-FUNCTION%20CL-ASTAR%3AMAKE-8-DIRECTIONS-ENUMERATOR) macro from `cl-astar` that handles 8-direction neghborhood (two vertical directions, two horizontal and four diagonal) to build upon using [`funcall`](https://cl-community-spec.github.io/pages/funcall.html) and pass the coordinates of tile start to it using our `tile-start` function.
* `exact-cost`: this function calculates the exact cost of moving between two neighboring tiles with given coordinates. Here if the target tile is an obstacle or movement involves crossing an obstacle diagonally, we return [`most-positive-single-float`](https://cl-community-spec.github.io/pages/most_002dpositive_002dshort_002dfloat.html) — the biggest single precision floating point number that signals to `cl-astar` that movement is impossible.
* `heuristic-cost`: the heuristic function to estimate the path cost between two arbitrary points with given coordinates. For our grid with diagonal movement, the octile distance (a generalization of Manhattan distance) would be a solid choice. A good overview of different metrics can be found on a great resource dedicated to A* algorithm, [Amit’s A* Pages](http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html). We use ready-made octile distance from the library, by calling the [`make-octile-distance-heuristic`](https://lockie.gitlab.io/cl-astar/#MACRO-FUNCTION%20CL-ASTAR%3AMAKE-OCTILE-DISTANCE-HEURISTIC) macro.
* `path-initiator`: the first of two functions that processes the found path. If a path has been found, it is called first with its length. We don't care about the length, so we ignore it by using [`ignorable`](https://cl-community-spec.github.io/pages/ignore.html) declaration. Within the function we check whether an entity already has a path with `has-path-p` predicate, and if it does, we go through all of its points obtained by calling the `path-points` index function with [`dolist`](https://cl-community-spec.github.io/pages/dolist.html) and delete them by calling [`ecs:delete-entity`](https://lockie.gitlab.io/cl-fast-ecs/#FUNCTION%20CL-FAST-ECS%3ADELETE-ENTITY). Then we assign the `destination-x` and `destination-y` slots of the `path` component to the values `goal-x` and `goal-y` by the `assign-path` function generated by ECS framework. If the `path` component did not exist on character entity before, it will be automatically created. The `goal-x` and `goal-y` are the parameters of the `find-path` function being defined.
* `path-processor`: the key function to process the result. It's called with the coordinates of each point of the found path, including the start and end points. In this function, for each point we create an entity with the `path-point` component having appropriate coordinates and `parent` component for the memory management reasons — if the character moving along the path is deleted, all of their path points should be deleted as well.

Finally, we modify the `handle-enemies` system so that enemies use the new pathfinding function instead of moving directly through solid objects toward the player:


```lisp
(ecs:defsystem handle-enemies
  (:components-ro (position size enemy)                                          ;; here
   :components-rw (character)
   :after (render-images)
   :with ((player-x player-y) := (with-position () (player-entity 1)
                                   (values x y))))
  (when (and (approx-equal position-x player-x enemy-vision-range)
             (approx-equal position-y player-y enemy-vision-range)               ;; and here
             (not (and (has-path-p entity)                                       ;;
                       (approx-equal                                             ;;
                        (path-destination-x entity) player-x size-width)         ;;
                       (approx-equal                                             ;;
                        (path-destination-y entity) player-y size-height))))     ;;
    (find-path position-x position-y player-x player-y                           ;;
               :entity entity :tile-width size-width :tile-height size-height))  ;;
  (when (and (approx-equal position-x player-x enemy-attack-range)
             (approx-equal position-y player-y enemy-attack-range)
             (not *should-quit*))
    (setf *should-quit* t)
    (al:show-native-message-box (cffi:null-pointer)
                                "ECS Tutorial 2" "" "You died"
                                (cffi:null-pointer) :warn)))
```

Now, when checking enemy's vision range, we also check if the enemy has a path to follow the player, and whether its destination point matches the player's coordinates to the nearest tile. If at least one of these conditions is false, we call the `find-path` function we've defined with the path's start coordinates (current `position-x` and `position-y`), its end coordinates (`player-x` and `player-y`) and the keyword arguments it needs.

Let's reload the system by calling `(ql:quickload :ecs-tutorial-2)` — we can't avoid it, since we've added a new library to the project's dependencies list. We will see that the enemies we've placed on the map do run after the player, while politely avoiding obstacles:

[![](uploads/0dde3c4a3b77726d9374b01e3ec0aacc/0.jpg)](https://youtu.be/z5aY4Zp3qFI)

For some reason, this kind of video immediately starts playing [Yakety Sax](https://youtu.be/lAJB6HsYiNA) in your head 😅

## Interfaces

Having finished the dungeon environment in which the player will act, we might want to add some narrative elements to the game. For this, we'll need a user interface, but traditional GUI libraries like Qt or GTK won't do since our controls will be rendered not in a standard OS window but on the graphical context created by `liballegro` which uses it to pass OpenGL commands to. Specialized libraries designed specifically for adding user interfaces to video games come to the rescue. There are plenty of [such libraries](https://github.com/raizam/gamedev_libraries#ui), but for our purposes, we'll choose the pure C library [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear), as it has a [Common Lisp binding](https://gitlab.com/lockie/cl-liballegro-nuklear) for use with `liballegro`, maintained by yours truly. Aside from providing access to library functions, this binding also offers a neat DSL for creating interfaces, fully aligned with the metalinguistic abstraction paradigm. The declarative interface currently includes the necessary minimum of tools, but otherwise it is a complete binding library.

To begin, let's add the library to our project dependencies in the `ecs-tutorial-2.asd` file. It's actually already included in the template, but we need to explicitly declare a dependency on the declarative interface, which lives in a separate package:

```lisp
  ;; ...
  :license "MIT"
  :depends-on (#:alexandria
               #:cl-fast-ecs
               #:cl-liballegro
               #:cl-liballegro-nuklear
               #:cl-liballegro-nuklear/declarative  ;; here
               #:cl-tiled
               #:let-plus
               #:livesupport)
  ;; ...
```

While we're here, let's reference a new file, `src/narrative.lisp`, which we'll also add to the project:

```lisp
  ;; ...
  :components ((:module "src"
              :components
              ((:file "package")
               (:file "common")
               (:file "animation")
               (:file "map")
               (:file "narrative")  ;; here
               (:file "character")
               (:file "main"))))
  ;; ...
```

In the `src/package.lisp` file, we'll add a nickname for the library so that instead of writing the long `cl-liballegro-nuklear/declarative:something` we can simply write `ui:something`:


```lisp
(defpackage #:ecs-tutorial-2
  (:use #:cl #:let-plus)
  (:import-from #:alexandria #:clamp #:define-constant #:make-keyword)
  (:import-from #:float-features #:single-float-nan #:float-nan-p)
  (:local-nicknames (#:tiled #:cl-tiled)
                    (#:ui #:cl-liballegro-nuklear/declarative))         ;; here
  (:export #:main))
```

Let's also add the *Alegreya* font to the `Resources` folder, which we'll use for the interface. You can download it from [Google Fonts](https://fonts.google.com/specimen/Alegreya+SC). Don't forget to rename the file from `AlegreyaSC-Regular.ttf` to `alegreya-sc.ttf` (we'll refer to it this way in the code for aesthetic reasons).

In the new `src/narrative.lisp` file, we will add the required header to activate our project's package and define a window where we will display our narratives:

```lisp
(in-package #:ecs-tutorial-2)


(ui:defwindow narrative (&key text)
    (:x (truncate +window-width+ 4) :y (truncate +window-height+ 4)
     :w (truncate +window-width+ 2) :h (truncate +window-height+ 2))
  (ui:layout-space (:height (truncate +window-height+ 4) :format :dynamic)
    (ui:layout-space-push :x 0.05 :y 0.15 :w 0.9 :h 1.2)
    (ui:label-wrap text)
    (ui:layout-space-push :x 0.5 :y 1.4 :w 0.4 :h 0.35)
    (ui:button-label "Ok"
      t)))
```

The `ui:defwindow` macro defines a function with the given name (`narrative`) and keyword arguments (`text`, which is where we expect the narrative text to be passed). This function displays a user interface window in our game with the specified parameters, particularly the screen coordinates `x` and `y`, width `w` and height `h`. Inside the function, we use the helper macro `ui:layout-space` and the `ui:layout-space-push` function to define the layout of widgets within the window. You can read more about layout mechanisms in (rather dry) [Nuklear documentation](https://immediate-mode-ui.github.io/Nuklear/doc/index.html#nuklear/api/layouting), but in practice, it's often easier to experiment when creating interfaces, as Lisp allows us to redefine the `narrative` function while the program is running by pressing magical keys `Ctrl-C Ctrl-C` (or others, depending on the IDE you're using).

[![](uploads/60718d9d768d6c9a2f8fdb362cc215d6/0.jpg)](https://youtu.be/gswpz8vNbj0)

On this window, we display two widgets — a text label created using `ui:label-wrap` ("wrap" indicates that the text will automatically wrap to the next line) and a button labeled *Ok*, created with the macro `ui:button-label`. This macro is peculiar: after the button label, we define the code that will run when the user clicks the button. In this case, we simply return the Boolean true, [`t`](https://cl-community-spec.github.io/pages/t.html) from the `narrative` function. When the button is rendered but not clicked, this macro automatically returns the Boolean false, [`nil`](https://cl-community-spec.github.io/pages/NILv.html). This behavior is dictated by the fact that Nuklear is so-called *immediate mode* library. Unlike the classic *retained mode*, where widgets are stored in separate objects in memory, immediate mode re-renders and processes everything at each frame. This make sense for game applications with their main game loop, where each frame is redrawn from scratch. As a result, we can't handle button clicks through methods or callbacks. Instead, we check for specific events, such as a button press, every time, and run the code accordingly.

Now, to make all this work, we need to modify `src/main.lisp` to initialize, run and shut down the `cl-liballegro-nuklear` UI library properly:

```lisp
(define-constant +font-path+ "inconsolata.ttf" :test #'string=)
(define-constant +font-size+ 24)
(define-constant +ui-font-path+ "alegreya-sc.ttf" :test #'string=)  ;; here
(define-constant +ui-font-size+ 28)                                 ;;

;; ...

(defun update (dt ui-context)                                       ;; here
  (unless (zerop dt)
    (setf *fps* (round 1 dt)))
  (ecs:run-systems :dt (float dt 0.0) :ui-context ui-context))      ;; and here

(defvar *font*)

(defun render ()
  (nk:allegro-render)                                               ;; here
  (al:draw-text *font* (al:map-rgba 255 255 255 0) 0 0 0
                (format nil "~d FPS" *fps*)))

;; ...

             (loop
               :named main-game-loop
               :with *font* := (al:ensure-loaded #'al:load-ttf-font
                                                 +font-path+
                                                 (- +font-size+) 0)
               :with ui-font := (al:ensure-loaded                          ;; here
                                 #'nk:allegro-font-create-from-file        ;;
                                 +ui-font-path+ (- +ui-font-size+) 0)      ;;
               :with ui-context := (nk:allegro-init ui-font display        ;;
                                                    +window-width+         ;;
                                                    +window-height+)       ;;
               :with ticks :of-type double-float := (al:get-time)
               :with last-repl-update :of-type double-float := ticks
               :with dt :of-type double-float := 0d0
               :while (loop
                        :named event-loop
                        :initially (nk:input-begin ui-context)             ;;
                        :while (al:get-next-event event-queue event)
                        :for type := (cffi:foreign-slot-value
                                      event '(:union al:event) 'al::type)
                        :do (nk:allegro-handle-event event)                ;;
                        :always (not (eq type :display-close))
                        :never *should-quit*
                        :finally (nk:input-end ui-context))                ;;

               ;; ...

                   (livesupport:continuable
                     (update dt ui-context)                                ;;
                     (narrative ui-context :text "Hello")                  ;;
                     (render))
                   (al:flip-display)
               :finally (nk:allegro-shutdown)                              ;;
                        (nk:allegro-font-del ui-font)                      ;;
                        (al:destroy-font *font*)))
```

After loading the accumulated changes in the system by running `(ql:quickload :ecs-tutorial-2)` and starting `(ecs-tutorial-2:main)`, we will see the narrative window we've defined:

![](uploads/43a293670547be19c8093766c727b7c0/ui-hello.png)

By default, the window looks quite dull, with the only nice feature being the font. Let's fix this by taking advantage of Nuklear's extensive style customization options (see, for example, the [gallery](https://github.com/Immediate-Mode-UI/Nuklear#gallery) on the project's github). First, download the [`kenney_fantasy-ui-borders.zip` archive](https://kenney.nl/assets/fantasy-ui-borders), which contains UI elements (if you can, feel free to donate to the author; otherwise, just click *"Continue without donating..."* to download for free). Extract the files `panel-000.png`, `panel-001.png` и `panel-031.png` from the `/PNG/Double/Panel` subdirectory, or choose others to your liking, and place them in your `Resource` folder. Then, we will process the last two files to add transparency using the following [ImageMagick](https://imagemagick.org/script/download.php) commands, executed in the same `Resources` folder:

```sh
convert panel-031.png -matte -channel A +level 40% +channel panel-031.png
cp panel-001.png panel-0011.png
convert panel-001.png -matte -channel A +level 40% +channel panel-001.png
```

Now, in the `src/narrative.lisp` file, let's define some global variables (for simplicity) to store the loaded images:

```lisp
(defvar *window-background*)
(defvar *button-normal-background*)
(defvar *button-hover-background*)
(defvar *button-active-background*)
```

Additionally, we'll define two functions — one for loading and another for unloading these images:

```lisp
(defun load-ui ()
  (flet ((load-ui-image (name)
           (al:ensure-loaded #'nk:allegro-create-image name)))
    (setf *window-background*        (load-ui-image "panel-031.png")
          *button-normal-background* (load-ui-image "panel-001.png")
          *button-active-background* (load-ui-image "panel-000.png")
          *button-hover-background*  (load-ui-image "panel-0011.png"))))

(defun unload-ui ()
  (nk:allegro-del-image *window-background*)
  (nk:allegro-del-image *button-normal-background*)
  (nk:allegro-del-image *button-active-background*)
  (nk:allegro-del-image *button-hover-background*))
```

In the `load-ui` function, we define a local function `load-ui-image` using [`flet`](https://cl-community-spec.github.io/pages/flet.html), which calls `nk:allegro-create-image` from `cl-liballegro-nuklear` to load an image in a format understood by the library. We also use [`al:ensure-loaded`](https://github.com/resttime/cl-liballegro/blob/eea31c9c949c1003d290db3f96564d9bffee0b8f/src/interface/streams.lisp#L15) to handle potential errors. We then use `load-ui-image` to load the images we need from the resources folder.

The `unload-ui` function frees the loaded images by calling `nk:allegro-del-image`, as these images are created on the C side, and Lisp garbage collector cannot free them.

Let's add calls to these functions in the `src/main.lisp` file, in the `init` and `main` functions:

```lisp
(defun init ()
  (ecs:bind-storage)
  (load-ui)                 ;; here
  (load-map "level1.tmx"))

;; ...

                   (al:flip-display)
               :finally (unload-ui)                    ;; and here
                        (nk:allegro-shutdown)
                        (nk:allegro-font-del ui-font)
                        (al:destroy-font *font*)))
```

Now we can use these images in our `narrative` function in the file `src/narrative.lisp` by utilizing the `styles` keyword argument of the `ui:defwindow` macro:

```lisp
(ui:defwindow narrative (&key text)
    (:x (truncate +window-width+ 4) :y (truncate +window-height+ 4)
     :w (truncate +window-width+ 2) :h (truncate +window-height+ 2)
     :styles ((:item-9slice :window-fixed-background *window-background*)   ;; here
              (:item-9slice :button-normal *button-normal-background*)      ;;
              (:item-9slice :button-hover *button-hover-background*)        ;;
              (:item-9slice :button-active *button-active-background*)      ;;
              (:color :text-color :r 0 :g 0 :b 0)                           ;;
              (:color :button-text-normal :r 0 :g 0 :b 0)                   ;;
              (:color :button-text-hover :r 0 :g 0 :b 0)                    ;;
              (:color :button-text-active :r 0 :g 0 :b 0)))                 ;;
  (ui:layout-space (:height (truncate +window-height+ 4) :format :dynamic)
    (ui:layout-space-push :x 0.05 :y 0.15 :w 0.9 :h 1.2)
    (ui:label-wrap text)
    (ui:layout-space-push :x 0.5 :y 1.4 :w 0.4 :h 0.35)
    (ui:button-label "Ok"
      t)))
```

After sending all new and modified forms to the Lisp process (`defvar` forms for images, `narrative`, `load-ui`, `unload-ui`, `init` and `main`), and restarting `ecs-tutorial-2:main`, we will see a much more aesthetically pleasing window:

![](uploads/4443e778e7e8750f27622a85c682807c/ui-styling.png)

Finally, using this window, we will implement *environmental storytelling* by creating objects that, when approached by the player's character, will display the corresponding text. Let's define the following component for those in `src/narrative.lisp`:

```lisp
(ecs:defcomponent narrative
  (text "" :type string)
  (shown nil :type boolean)
  (active nil :type boolean :index active-narratives))
```

In this component, besides the `text` slot for the narrative text, we will store a Boolean flag `shown` to track whether the window with this text has already been shown to the player. We don't want to display the text *every* time the player passes by an object; instead, the player will need to press a key, for instance *E*, to read the text again. Additionally, we define another Boolean slot, `active`, which will indicate whether the narrative window is currently being displayed on the screen. We also define an index on this slot, `active-narratives`, so we can check if any narrative window is currently active. When a narrative is displayed, we will pause all active processes, such as character movement, allowing the player to read the text in peace.

Now, let's remove the demo call `(narrative ui-context :text "Hello")` from the `main` function in `src/main.lisp` and define a system to display narrative objects:

```lisp
(defconstant +interact-distance-factor+ 1.2)

(ecs:defsystem show-narrative
  (:components-ro (position)
   :components-rw (narrative)
   :arguments ((ui-context cffi:foreign-pointer))
   :with ((player-x player-y dx dy) :=
          (let ((player (player-entity 1)))
            (values
             (position-x player)
             (position-y player)
             (* +interact-distance-factor+ (size-width player))
             (* +interact-distance-factor+ (size-height player))))))
  (al:with-current-keyboard-state keyboard-state
    (when (and (approx-equal position-x player-x dx)
               (approx-equal position-y player-y dy)
               (or narrative-active
                   (not narrative-shown)
                   (al:key-down keyboard-state :E)))
      (setf narrative-shown t
            narrative-active t)
      (when (or (narrative ui-context :text narrative-text)
                (al:key-down keyboard-state :escape)
                (al:key-down keyboard-state :space)
                (al:key-down keyboard-state :enter))
        (setf narrative-active nil)
        (when (has-win-p entity)
          (setf *should-quit* t))))))
```

First, we define the constant `+interact-distance-factor+`, which is an experimentally fine-tuned multiplier of the tile size used to calculate the interaction distance from a narrative object, determining when its window will open. For the system, we use the `with` argument to retrieve the player's coordinates into the variables `player-x` and `player-y`, as well as the player's tile size, multiplied by `+interact-distance-factor+`, into the variables `dx` and `dy`. The entire system code is wrapped in the familiar `with-current-keyboard-state` macro from the `cl-liballegro` library, which allocates a C struct [`ALLEGRO_KEYBOARD_STATE`](https://liballeg.org/a5docs/trunk/keyboard.html#allegro_keyboard_state) on the stack, fills it with the current keyboard state and puts to the pointer we call `keyboard-state`.

Then, for each narrative object, we check if it's within the interaction distance by calling our old familiar `approx-equal`. Additionally, we check three conditions:

1. The narrative window is currently active,
2. The window hasn't been shown before,
3. The interaction key (*E*) in the keyboard is pressed (checked using [`al_key_down`](https://liballeg.org/a5docs/trunk/keyboard.html#al_key_down)).

At least one of these conditions must be true to start displaying the narrative window. If it's time to show the window, we set the `shown` and `active` flags of the narrative object to Boolean true value [`t`](https://cl-community-spec.github.io/pages/t.html), and invoke the `narrative` function, passing the `ui-context` (kindly provided by the `update` function) and the text from the `text` slot of the narrative component. We then check the return value of the function (it returns a true value if the *Ok* button is pressed, and false otherwise), as well as whether one of the keys *Esc*, *Space*, or *Enter* has been pressed. If any of these conditions are met, we close the window by setting `active` slot to false, [`nil`](https://cl-community-spec.github.io/pages/NILv.html), ensuring the window will not be displayed on the next main game loop iteration.

Now we just need to add custom `narrative` type in Tiled's custom type editor, with a `text` member of type `string`:

![](uploads/7624d6645c546fd448939df883d545de/narrative-type-en.png)

...and assign this property (also named `narrative`) to some objects on the object layer. There's a small catch related to the collision detection mechanism: if we want the object to be impassable for characters, its coordinates must be aligned with the tile grid. This can be ensured by manually setting the *X* and *Y* properties in the object's property editor:

![](uploads/82167b7c0252cc4ac56337331a0cffba/narrative-obstacle-en.png)

After sending the constant `+interact-distance-factor+` definition, the `narrative` component, the `show-narrative` system, and the modified `main` function to the Lisp process and restarting the latter, we can see our environmental storytelling in action:

[![](uploads/ce4787a957c5638502cfab960f41f31a/0.jpg)](https://youtu.be/D37MVnGXedw)

However, you'll notice that when the narrative window is displayed, the dungeon continues to live its life — enemies still chase the player, and the player can move freely. To give the player a chance to enjoy reading the text without interruptions, we'll modify the `move-characters` and `control-player` systems as follows:

```lisp
(ecs:defsystem move-characters
  (:components-rw (position character)
   :components-ro (size)
   :when (null (active-narratives t))   ;; here
   :arguments ((dt single-float)))
   ;; ...

(ecs:defsystem control-player
  (:components-ro (player position size)
   :components-rw (character)
   :after (move-characters)
   :when (null (active-narratives t)))  ;; here
   ;; ...
```

Using the `when` argument, we allow these systems to execute only when the index function `active-narratives` returns [`nil`](https://cl-community-spec.github.io/pages/NILv.html) when called with the [`t`](https://cl-community-spec.github.io/pages/t.html) argument, i.e. when no narrative window is active.

And finally, as the finishing touch, let's add a victory condition to the game since we already have game over condition. To do this, we'll add another tag component called `win`:

```lisp
(ecs:defcomponent win)
```

We'll assume this component accompanies the `narrative` component, which will display a text congratulating the player for winning. After pressing *Ok* in such a narrative window, the game should end. Let's make the necessary changes to the `show-narrative` system:

```lisp
(ecs:defsystem show-narrative
  (:components-ro (position)
   :components-rw (narrative)
   :arguments ((ui-context cffi:foreign-pointer))
   :with ((player-x player-y dx dy) :=
          (let ((player (player-entity 1)))
            (values
             (position-x player)
             (position-y player)
             (* +interact-distance-factor+ (size-width player))
             (* +interact-distance-factor+ (size-height player))))))
  (al:with-current-keyboard-state keyboard-state
    (when (and (approx-equal position-x player-x dx)
               (approx-equal position-y player-y dy)
               (or narrative-active
                   (not narrative-shown)
                   (al:key-down keyboard-state :E)))
      (setf narrative-shown t
            narrative-active t)
      (when (or (narrative ui-context :text narrative-text)
                (al:key-down keyboard-state :escape)
                (al:key-down keyboard-state :space)
                (al:key-down keyboard-state :enter))
        (setf narrative-active nil)
        (when (has-win-p entity)                                      ;; here
          (setf *should-quit* t))))))                                 ;;
```

We'll also add the new component in the Tiled custom types editor:

![](uploads/f0ae2a71373d7ab2b608a5d89971bc64/win-type-en.png)

At this point, the game reaches its final form — overcoming difficult obstacles and learning about the world, the player eventually achieves victory 🏆

## Conclusion

Well, looking back at the long journey we've undertaken, we've explored the capabilities of the Common Lisp ECS framework `cl-fast-ecs`, while also getting familiar with new libraries like `cl-tiled`, `cl-astar` and `cl-liballegro-nuklear`. In the end, we implemented a Souls-like dungeon crawler with environmental storytelling, enemy AI, and GUI in just about five hundred lines of code. This highlights the advantages of the metalinguistic abstraction and Entity-Component-System architecture (and data-driven approach in general). Of course, the component and system design presented here is by no means the only correct approach, but it serves as a humble starting point for your own creative endeavors. The full code for our dungeon crawler can be found on [github](https://github.com/lockie/ecs-tutorial-2). In addition to the code reviewed in this tutorial, it also includes optional type declarations using the [`declaim`](https://cl-community-spec.github.io/pages/declaim.html) macro, helping the compiler generate slightly more optimized code and squeeze several thousands FPS out of it 😊

Many important aspects of game development remain outside the scope of this tutorial, such as [sound design](https://liballeg.org/a5docs/trunk/audio.html), [cutscenes](https://liballeg.org/a5docs/trunk/video.html), main menu, level transitions, the infamously tricky [door problem](https://lizengland.com/blog/2014/04/the-door-problem), and many more. Hopefully this tutorial will serve well as a helpful example for implementing all the necessary mechanics.

If you want to give making Lisp games a shot and get feedback from community, I invite you to participate in the [Autumn Lisp Game Jam 2024](https://itch.io/jam/autumn-lisp-game-jam-2024) online event, which will take place on the indie developer platform [itch.io](https://itch.io) next week, on October 25th 2024 (and don't worry if you're reading this in the distant future and have missed this event — it happens regularly twice a year, in April and in October). During this game jam, you create a game over the course of 10 days using any Lisp dialect (I'd of course recommend Common Lisp), and then you vote on your peers' work and receive feedback on your own. This part of the tutorial was, in fact, based on the game [Thoughtbound](https://itch.io/jam/spring-lisp-game-jam-2023/rate/2102758), created for the Spring Lisp Game Jam 2023.

In the next part of this tutorial, we'll scale things up, add more advanced AI, and take on the challenge of creating a real-time strategy game. Follow me on [itch.io](https://awkravchuk.itch.io) so you don't miss the next part.

I'd like to thank my friend [@ViruScD](https://viruscd.itch.io) for support and help in writing the article, and [Grigory](https://fosstodon.org/@shegeley) and liltechdude for help with proofreading the text.